﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBoxFixer : MonoBehaviour {

    [SerializeField] private RectTransform textBlock, choice1, choice2, choice3;

	// Use this for initialization
	void Start () {
        textBlock.anchoredPosition = new Vector2(48f, 0f);
        choice1.anchoredPosition = new Vector2(38f, 18f);
        choice2.anchoredPosition = new Vector2(38f, -13f);
        choice3.anchoredPosition = new Vector2(38f, -42f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

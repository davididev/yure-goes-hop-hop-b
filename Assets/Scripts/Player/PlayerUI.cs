﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    [SerializeField] private TMPro.TextMeshProUGUI timerText;
    [SerializeField] private Image healthImage, fireballPowerupBar, jumpPowerupBar, invincPowerupBar;
    [SerializeField] private Animator healthAnim, timerAnim, catAnim;
    [SerializeField] private GameObject pauseMenu, fireballPowerupBase, jumpPowerupBase, invincPowerupBase, livesThingy;

	// Use this for initialization
	void Start () {
        StartCoroutine(Timer());
        DoTheLivesThingy();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void DoTheLivesThingy()
    {
        for(int i = 0; i < 9; i++)
        {
            GameObject g = livesThingy.transform.GetChild(i).gameObject;
            g.SetActive(GameVars.lives > i);
        }
    }
	
	//UI transition for loading new level
	public void LoadNewLevel()
	{
		StartCoroutine(LoadingNewScene());
	}
	
	public IEnumerator LoadingNewScene(float delay = 0f)
	{
        yield return new WaitForSecondsNoTimeScale(delay);
        catAnim.SetTrigger("End Level");
		yield return new WaitForSecondsNoTimeScale(1f);
		SceneManager.LoadScene("LoadingScene");
	}
	
	// Update is called once per frame
	void Update () {
        UpdateHealth();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeInHierarchy);
            if (pauseMenu.activeSelf == true)
            {
                Time.timeScale = 0f;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            if (pauseMenu.activeSelf == false)
			{
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                Time.timeScale = 1f;
			}
        }

        if(PlayerSword.swordPerc > 0f)
        {
            fireballPowerupBase.SetActive(true);
            fireballPowerupBar.fillAmount = PlayerSword.swordPerc;
            fireballPowerupBar.color = Color.Lerp(Color.red, Color.green, PlayerSword.swordPerc);
        }
        else
        {
            fireballPowerupBase.SetActive(false);
        }

        if (PlayerPhysics.doubleJumpTimer > 0f)
        {
            jumpPowerupBase.SetActive(true);
            jumpPowerupBar.fillAmount = PlayerPhysics.doubleJumpPerc;
            jumpPowerupBar.color = Color.Lerp(Color.red, Color.green, PlayerPhysics.doubleJumpPerc);
        }
        else
        {
            jumpPowerupBase.SetActive(false);
        }

        if (PlayerPhysics.invincibilityPerc > 0f)
        {
            invincPowerupBase.SetActive(true);
            invincPowerupBar.fillAmount = PlayerPhysics.invincibilityPerc;
            invincPowerupBar.color = Color.Lerp(Color.red, Color.green, PlayerPhysics.invincibilityPerc);
        }
        else
        {
            invincPowerupBase.SetActive(false);
        }
    }

    public void Unpause()
    {
        StartCoroutine(UnpauseRoutine());
    }

    private void OnEnable()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    IEnumerator UnpauseRoutine()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        yield return new WaitForSecondsNoTimeScale(1f);
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void RestartLevel()
    {
        StartCoroutine(RestartLevelRoutine());
    }

    IEnumerator RestartLevelRoutine()
    {
        yield return new WaitForSecondsNoTimeScale(1f);
        FindObjectOfType<PlayerUI>().LoadNewLevel();
        //Restart the level
		
    }

    public void QuitGame()
    {
        StartCoroutine(QuitGameRoutine());
    }

    public void GoToTitle()
    {
        LoadingUI.scene = "Title";
        StartCoroutine(LoadingNewScene(1f));
    }

    IEnumerator QuitGameRoutine()
    {
        yield return new WaitForSecondsNoTimeScale(1f);
        FindObjectOfType<PlayerPhysics>().GoToWorldSelection();
        //Quit the level
    }

    public static int timer = 900;

    IEnumerator Timer()
    {
        while(timer > 0)
        {
            timerText.text = "Time: " + timer.ToString("D3");
            if(PlayerPrefs.GetInt("CheatTime") == 0)
                timer--;
            timerAnim.SetBool("time_low", timer <= 30);
            if(timer == 30)
            {
                AudioSource a = Camera.main.GetComponent<AudioSource>();
                a.pitch = 2f;
            }

            yield return new WaitForSeconds(1f);
            while(DialogueHandler.IS_RUNNING)
            { yield return new WaitForEndOfFrame(); }
        }
        timerText.text = "Time up!";

        //Game over.  Heh.  Hehhehehehehe.
        while (Health.playerHealth > 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().DamageOverride(1);
            yield return new WaitForSeconds(1f);
        }
    }

    void UpdateHealth()
    {
        healthImage.fillAmount = (Health.playerHealth * 100 / 6) / 100f;
        if (Health.playerHealth >= 5 && Health.playerHealth <= 6)
            healthImage.color = Color.green;
        if (Health.playerHealth >= 3 && Health.playerHealth <= 4)
            healthImage.color = Color.yellow;
        if (Health.playerHealth <= 2)
            healthImage.color = Color.red;

        healthAnim.SetBool("health_low", Health.playerHealth <= 2);
    }
}

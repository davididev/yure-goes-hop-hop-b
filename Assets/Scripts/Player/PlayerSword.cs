﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSword : MonoBehaviour {

    public static bool isSwinging = false;
    private Coroutine swordRoutine;
    private BoxCollider box;
    [SerializeField] private GameObject fireballPrefab;
    private PlayerFireball[] fireballs;
    private const int FIREBALL_COUNT = 80;
    private const float FIREBALL_SPAWN_COUNT_TIMER = 0.15f;

    public static float swordPerc { get; private set; }

	// Use this for initialization
	void Start () {
        swordPerc = 0f;
        fireballs = new PlayerFireball[FIREBALL_COUNT];
        for(int i = 0; i < fireballs.Length; i++)
        {
            GameObject g = GameObject.Instantiate(fireballPrefab, transform.position, transform.rotation, null) as GameObject;
            fireballs[i] = g.GetComponent<PlayerFireball>();
            g.SetActive(false);
            
        }

        //For testing purposes
        //StartFireballRoutine();
	}

    public void StartFireballRoutine()
    {
        if (swordRoutine != null)
            StopCoroutine(swordRoutine);
        swordRoutine = StartCoroutine(FireballRoutine());
    }

    public IEnumerator FireballRoutine()
    {
        float timer = LevelVars.POWERUP_TIME;
        while(timer > 0f)
        {
            swordPerc = timer / LevelVars.POWERUP_TIME;
            SpawnFireball();
            yield return new WaitForSeconds(FIREBALL_SPAWN_COUNT_TIMER);
            timer -= FIREBALL_SPAWN_COUNT_TIMER;
        }

        swordPerc = 0f;
    }

    void SpawnFireball()
    {
        for (int i = 0; i < fireballs.Length; i++)
        {
            if (fireballs[i].gameObject.activeSelf == false)
            {
                fireballs[i].gameObject.SetActive(true);
                fireballs[i].transform.position = transform.position;
                fireballs[i].transform.forward = transform.up;
                break;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (box == null)
            box = GetComponent<BoxCollider>();

        if(!dropped)
            box.enabled = isSwinging;
	}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("I'm sorry, I couldn't hear you over the sound of " + other.gameObject.name);
        if(other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Health h = other.GetComponent<Health>();
            if(h != null)
            {
                h.Damage(1, transform);
            }
        }
    }

    bool dropped = false;
    public void DropSword()
    {
        dropped = true;
        box.enabled = true;
        box.isTrigger = false;
        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.isKinematic = false;
        rigid.AddForce(new Vector3(0f, -100f, 0f));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireball : MonoBehaviour {

    private Rigidbody rigid;
    private const float MOVE_SPEED = 10f;
    private const float TIMER = 2.0f;
    private float time = 0f;

    public Material ashMat;

    // Use this for initialization
    void OnEnable () {
        time = TIMER;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(rigid == null)
            rigid = GetComponent<Rigidbody>();

        rigid.velocity = MOVE_SPEED * transform.forward;
        time -= Time.fixedDeltaTime;
        if (time < 0f)
            gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {

        Health h = collision.gameObject.GetComponent<Health>();
        if (h != null)
        {
            h.Damage(1, GameObject.FindGameObjectWithTag("Player").transform);
            gameObject.SetActive(false);
        }

        int layer = collision.gameObject.layer;
        if (LayerMask.LayerToName(layer) == "Default")
        {
            //GameObject ash = GameObjectPool.GetInstance("decal", Vector3.zero, Quaternion.identity);
            //ash.GetComponent<Decal>().SetupDecal(collision.contacts[0].point, collision.contacts[0].normal, ashMat, 1f, 0.5f, 2f, Decal.KillType.SHRINK);
        }
        
    }
}

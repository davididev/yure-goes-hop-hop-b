﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;

//Need this in android manifest: <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

public class TakeSnapshot : MonoBehaviour {

    [SerializeField] private TMPro.TextMeshProUGUI screenshotSavedText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.F12))
        {
            Snapshot();
        }
	}

    

    void Snapshot()
    {
        screenshotSavedText.color = new Color(1f, 1f, 1f, 0f);

        DateTime currentDate = new DateTime();
        currentDate = DateTime.Now;
        

        
        
        
        string fileName = currentDate.ToShortDateString().Replace("/", "-")  + " " + currentDate.ToShortTimeString().Replace(":", "-") + currentDate.Millisecond.ToString("D4");
        
		
		
        string oldDir = "./";

		string newDir = "";
        if (!Application.isMobilePlatform)
            newDir = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        if (Application.platform == RuntimePlatform.Android)
        { 
            if(Directory.Exists("sdcard/DCIM/camera/"))
                newDir = "/sdcard/DCIM/camera/";
            else
                newDir = "/sdcard/media/images/";
        }
        
		/*

        yield return new WaitForSeconds(0.05f);


        TakeScreenshot(Path.Combine(newDir, fileName));

        //Force refresh
        if (Application.platform == RuntimePlatform.Android)
        {
            //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
            AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + newDir) });
            objActivity.Call("sendBroadcast", objIntent);
            //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE
        }

        yield return new WaitForSeconds(0.05f);
        */

		if (Application.isMobilePlatform)  //Only turn VR mode off and on in mobile form
			StartCoroutine(ScreenShotBridge.SaveScreenShot(fileName + ".png", "Yure Hop", true, OnScreenshot));
		else
		{
			StartCoroutine(TakeScreenshot(fileName, newDir));
		}

        



    }

    private void OnScreenshot(bool value)
    {
        if (value == true)
            CompleteMessage("Screenshot saved successfully");
        else
            CompleteMessage("<color=red>Screenshot failed to save</color>");

    }

    private void CompleteMessage(string s)
    {
        screenshotSavedText.text = s;
        iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 0.5f, "onupdate", "SetAlpha", "ignoretimescale", true, "oncomplete", "FadeStep2"));
        
    }

    void FadeStep2()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 1f, "time", 3f, "onupdate", "SetAlpha", "ignoretimescale", true, "oncomplete", "FadeStep3"));
    }

    void FadeStep3()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 0.5f, "onupdate", "SetAlpha", "ignoretimescale", true));
    }

    
    private IEnumerator TakeScreenshot(string fileName, string screen_path)
    {
		
		string origin = Application.persistentDataPath + "/" + fileName + ".png";
			ScreenCapture.CaptureScreenshot(origin);
			//string origin = System.IO.Path.Combine(Application.persistentDataPath, fileName + ".png");
			int failCount = 0;
			while(File.Exists(origin) == false)
			{
				yield return new WaitForSecondsNoTimeScale(0.1f);
				failCount++;
				if(failCount > 25)
				{
					//Let it fail
					break;
				}
			}
			string newDir = screen_path + "/" + fileName + ".png";
		
		try
		{
			Debug.Log("Attempting to move Screenshot (" + origin + ") to new dir (" + newDir + ")saved!");
			System.IO.File.Move(origin, newDir);
		
			Debug.Log("Screenshot (" + newDir + ") saved!");
			CompleteMessage("Screenshot saved successfully to My Pictures");
		}
		catch(System.Exception ear)
		{
			Debug.Log("Screenshot failed to save: " + ear.ToString());
			CompleteMessage("<color=red>Screenshot failed to save</color>");
		}
    }
    
    void SetAlpha(float a)
    {
        Color c = screenshotSavedText.color;
        c.a = a;
        screenshotSavedText.color = c;
    }
}

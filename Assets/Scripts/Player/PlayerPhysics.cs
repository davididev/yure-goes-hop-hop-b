﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerPhysics : MonoBehaviour {

    public TextAsset textTest;
    private CharacterController rigid;
    [SerializeField] private Transform leftHand, rightHand;
    [SerializeField] private GameObject swordPrefab, diePrefab, decalPrefab;
    [SerializeField] private AudioClip jumpClip, swingClip;

    [SerializeField] private Animator yureAnim;
    private const float MOVE_SPEED = 8f;  //Acceleration
    private const float SLOWDOWN_SPEED = 24f;
    private const float MAX_VELOCITY = 6f;  //How fast you can walk
    private const float JUMP_FORCE = 35f;  //Jump force
    private const float JUMP_HEIGHT = 15f;
    private const float CAM_DISTANCE = 5f;  //How far the camera should be from the player
    private const float CAM_ROTATE_PER_SECOND = 90f;  //How fast the camera spins
    private const float PLAYER_ROTATE_PER_SECOND = 960f;  //How fast the player turns

    [SerializeField] private ParticleSystem invincibility;

    public int airJumps = 0;
    public static float doubleJumpTimer = 0f;
    public static float doubleJumpPerc = 0f, invincibilityPerc = 0f;

    private Vector3 startPos;

    private float ultima_invinc_timer = 0f;

	public TMPro.TextMeshProUGUI checkpointText;
    public Material crackDecal;
	
    /// <summary>
    /// Called by the intro
    /// </summary>
    public void GoToWorldSelection()
    {
        LoadingUI.scene = "WorldSelection";
        GameObject.FindObjectOfType<PlayerUI>().LoadNewLevel();
		//UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
        //Later on, we will replace this with the world selection screen
    }

    /// <summary>
    /// Message for the Dialogue System.
    /// </summary>
    /// <param name="f"></param>
    public void SetMoveSpeed(float f)
    {
        yureAnim.SetFloat("speed", f);
    }

    // Use this for initialization
    void Start () {
        if(PlayerPrefs.GetInt("CheatGod") == 1)
        {
            ultima_invinc_timer = 20f;
            BecomeInvincible();
        }
        
        GameObjectPool.InitPoolItem("decal", decalPrefab, 50);
        //StartCoroutine(Text());
        doubleJumpTimer = 0f;
        invincibilityPerc = 0f;
        startPos = transform.position;

        //BecomeInvincible();

        if (PlayerPrefs.GetInt("CheatPowerup") == 1)
            powerupScale = 0.25f;

    }

    public void StartDoubleJump()
    {
        doubleJumpTimer = LevelVars.POWERUP_TIME;
    }

    Coroutine inv;
    public void BecomeInvincible()
    {
        if (inv != null)
            StopCoroutine(inv);
        inv = StartCoroutine(Invincibility());
    }

    public IEnumerator Invincibility()
    {
        Health h = GetComponent<Health>();
        h.invincible = true;
        invincibility.Play();
        float timer = LevelVars.POWERUP_TIME;
        while(timer > 0f)
        {
            timer -= Time.deltaTime * powerupScale;
            invincibilityPerc = timer / LevelVars.POWERUP_TIME;
            yield return new WaitForEndOfFrame();

        }
        invincibilityPerc = 0f;
        h.invincible = false;
        invincibility.Stop();
    }



    IEnumerator Text()
    {
        yield return new WaitForSeconds(0.5f);
        FindObjectOfType<DialogueHandler>().StartEvent(textTest);
    }

    private void Awake()
    {
        GameObject s1 = GameObject.Instantiate<GameObject>(swordPrefab);
        s1.transform.parent = leftHand;
        s1.transform.localPosition = new Vector3(-0.5f * 0.01f, 0.015f, 0f);
        s1.transform.localEulerAngles = new Vector3(0f, 0f, 270f);


        GameObject s2 = GameObject.Instantiate<GameObject>(swordPrefab);
        s2.transform.parent = rightHand;
        s2.transform.localPosition = new Vector3(-0.5f * 0.01f, 0.01f, 0f);
        s2.transform.localEulerAngles = new Vector3(0f, 0f, 90f);
		
		if(GlobalData.instance.easterEggProgress == 3)
			ActivateEgg();
    }
	
	public void ActivateEgg()
	{
		infiniteJump = true;
	}


    private float currentSpeed = 0f;
    /// <summary>
    /// Physics calcalations
    /// </summary>
    void FixedUpdate () {
        #if UNITY_EDITOR
		  if(Input.GetKeyDown(KeyCode.G))
		  {
			  infiniteJump = true;
		  }
		#endif
		
		//infiniteJump = true;  //TEMP
        if (infiniteJump)
            airJumps = 50;



        if (rigid == null)
            rigid = GetComponent<CharacterController>();
        rigid.detectCollisions = true;



        //Don't allow controls during dialogue handling, but allow things such as slowdown.
        Vector3 relMovement = new Vector3(0f, 0f, 0f);
        if (DialogueHandler.IS_RUNNING == false)
        {
            
            //relMovement.z = Input.GetAxis("Vertical") * MOVE_SPEED;
            //relMovement.x = Input.GetAxis("Horizontal") * MOVE_SPEED;
            if (Input.GetAxis("Vertical") != 0)
                relMovement.z = MOVE_SPEED;
            if (Input.GetAxis("Horizontal") != 0f)
                relMovement.z = MOVE_SPEED;
            if(relMovement == Vector3.zero)
            {
                currentSpeed -= SLOWDOWN_SPEED * Time.fixedDeltaTime;
                if (currentSpeed < 0f)
                    currentSpeed = 0f;
            }
            else
            {
                currentSpeed += MOVE_SPEED * Time.fixedDeltaTime;
                if (currentSpeed > MAX_VELOCITY)
                    currentSpeed = MAX_VELOCITY;
            }
            if (h == null)
                h = GetComponent<Health>();
            if (this.h.healthPerc == 0f)
            {
                currentSpeed = 0f;
                return;
            }

            Jump();
            Gravity();
            Vector3 v = ((transform.forward * currentSpeed) + (Vector3.up * gravity)) * Time.fixedDeltaTime;
            rigid.Move(v);
        }

    }
    Health h;

    void Gravity()
    {
        if(jumpHeight > 0.0f)
        {
            if (gravity < 0f)
                gravity = 0f;
            float f = JUMP_FORCE * Time.fixedDeltaTime;
            if (f > jumpHeight)
                f = jumpHeight;

            gravity += f;
            jumpHeight -= f;
            
        }
        else
        {
            gravity -= 20f * Time.fixedDeltaTime;
            if (rigid.isGrounded == true)
            {
                if (crack == false)
                {
                    crack = true;
                    //Crack decal!
                    RaycastHit info;
                    if (Physics.Raycast(transform.position, Vector3.down, out info, 1f, LayerMask.GetMask("Default")))
                    {
                        float decalSize = 1.5f;
                        decalSize += (Mathf.Abs(gravity) / 8f);
                        GameObject crack = GameObjectPool.GetInstance("decal", Vector3.zero, Quaternion.identity);
                        crack.GetComponent<Decal>().SetupDecal(info.point, info.normal, crackDecal, decalSize, 3f, 2f, Decal.KillType.INSTANT_HIDE);
                    }

                }
                gravity = -0.1f;
            }
            if(rigid.isGrounded == false)
                crack = false;
        }

    }

    bool crack = false;

    public static bool infiniteJump = false;  //Easter egg I will put somewhere in the game >:3
    private float jumpTimer = 0f;
    private bool isGrounded = false;
    private float gravity = 0f;
    bool previousJump = false;
    public float jumpHeight = 0f;
    void Jump()
    {

        if (Input.GetAxisRaw("Jump") != 0f)
        {
            RaycastHit myInfo;
            if (Physics.Raycast(transform.position, transform.forward, out myInfo, 5f, LayerMask.GetMask("NPC")))
            {
                SpeakingNPC npc = myInfo.transform.GetComponent<SpeakingNPC>();
                if (npc != null)
                {
                    if (npc.SpeakToNPC(transform) == true)
                        return;
                }
            }
        }

        isGrounded = rigid.isGrounded;
        if (isGrounded && doubleJumpTimer > 0f)
            airJumps = 1;

        if (jumpTimer > 0f)
        {
            jumpTimer -= Time.deltaTime * powerupScale;
            return;
        }

        if (previousJump == true && Mathf.Approximately(Input.GetAxisRaw("Jump"), 0) == true)
            previousJump = false;

        if (!previousJump && Mathf.Approximately(Input.GetAxisRaw("Jump"), 0) == false && jumpTimer <= 0f)
        {
            previousJump = true;
            Debug.Log("Grounded: " + isGrounded + "; air jumps: " + airJumps);
            if (isGrounded == false)
            {
                if(airJumps <= 0)
                {
                    return;
                }
                airJumps -= 1;
            }

            

            AudioSource.PlayClipAtPoint(jumpClip, transform.position);

            //Either infinite jumps are on, or you are on the ground.  Let's jump!
            jumpHeight = JUMP_HEIGHT;
            jumpTimer = 0.15f;
        }
		//Start falling early if you let go of space.
		if (Input.GetAxisRaw("Jump") == 0f)
		{
			jumpHeight = 0f;
		}
    }
    public static float attackTimer = 0f;
    void Animation()
    {
        if (attackTimer > 0f)
            attackTimer -= Time.deltaTime;
        yureAnim.SetFloat("speed", rigid.velocity.magnitude);
        yureAnim.SetBool("inAir", !rigid.isGrounded);

        bool attack = false;
        if (Input.GetKeyDown(KeyCode.Mouse0))
            attack = true;
        if (Input.GetKeyDown(KeyCode.LeftControl))
            attack = true;
        if (Input.GetKeyDown(KeyCode.RightControl))
            attack = true;
        if (Input.GetKeyDown(KeyCode.Z))
            attack = true;
        if (Input.GetKeyDown(KeyCode.Slash))
            attack = true;

        if (attack == true && attackTimer <= 0f)
        {
            
            StartCoroutine(Swing());
            
        }
    }

    IEnumerator Swing()
    {
        attackTimer = 0.5f;
        AudioSource.PlayClipAtPoint(swingClip, transform.position);
        PlayerSword.isSwinging = true;
        yureAnim.SetTrigger("attack");
        yield return new WaitForSeconds(0.5f);
        PlayerSword.isSwinging = false;
    }

    float currentAngle = 0f;
    float currentHeight = 0f;
    float powerupScale = 1f; //Offset of powerup; should only be changed via cheats
    /// <summary>
    /// Regular update functions
    /// </summary>
    private void Update()
    {

        if (ultima_invinc_timer > 0f)
        {
            ultima_invinc_timer -= Time.deltaTime;
            if (ultima_invinc_timer <= 0f)
            {
                ultima_invinc_timer = 20f;
                BecomeInvincible();
            }
        }
        doubleJumpTimer -= Time.deltaTime * powerupScale;
            doubleJumpPerc = doubleJumpTimer / LevelVars.POWERUP_TIME;
        
        //Don't allow controls during dialogue
        if(DialogueHandler.IS_RUNNING == false)
        {
            Animation();
            Transform t = Camera.main.transform;
            Vector3 relCameraPos = transform.InverseTransformPoint(t.position);
            //float newAngle = Mathf.LerpAngle(currentAngle, currentAngle + (Input.GetAxis("Mouse X") * CAM_ROTATE_PER_SECOND), Time.deltaTime);
            float newAngle = currentAngle + (Input.GetAxis("Mouse X") * CAM_ROTATE_PER_SECOND) * Time.deltaTime;
            if (newAngle > 360f)
                newAngle -= 360f;
            if (newAngle < 0f)
                newAngle += 360f;

            newAngle = Mathf.LerpAngle(currentAngle, newAngle, 1f);
            //Debug.Log("Previous angle: " + currentAngle + "; new angle: " + newAngle);

            currentAngle = newAngle;  //Update it!



            relCameraPos.x = CAM_DISTANCE * Mathf.Cos(newAngle * Mathf.Deg2Rad);
            relCameraPos.z = CAM_DISTANCE * Mathf.Sin(newAngle * Mathf.Deg2Rad);

            Vector3 raycastStartingPos = transform.position;
            Vector3 dir = (Camera.main.transform.position - transform.position).normalized;

            RaycastHit outInfo;
            bool hitAWall = false;
            if (Physics.Raycast(raycastStartingPos, dir, out outInfo, CAM_DISTANCE, LayerMask.GetMask("Default")))
            {
                hitAWall = true;
                //Is the camera hitting a wall?  Let's bring it a liiiittle bit closer to the player.
                float d = outInfo.distance;
                d -= 0.5f;  //So it's not EXACTLY on the wall...bring it a tiny bit closer so you don't see void.
                relCameraPos = relCameraPos.normalized * d;
            }

            //Note to self- Make MouseX respond to keyboard arrow keys and right joystick as well
            //Noted.  :D

            if (hitAWall == false)
            {
                currentHeight = currentHeight + (Input.GetAxis("Mouse Y") * Time.deltaTime * 5f);
                currentHeight = Mathf.Clamp(currentHeight, 0f, 10f);
            }
            relCameraPos.y = currentHeight;

            t.position = transform.position + relCameraPos;
            t.LookAt(transform);


            //Player rotation- thou shalt need this
            //Mathf.MoveTowardsAngle

            //Get rotation according to camera angle and Horizontal/Vertical
            if (!(Mathf.Approximately(Input.GetAxisRaw("Horizontal"), 0f) && Mathf.Approximately(Input.GetAxisRaw("Vertical"), 0f)))
            {
                float rel = 0f;
                rel = Mathf.Atan2(-Input.GetAxisRaw("Vertical"), Input.GetAxisRaw("Horizontal")) * Mathf.Rad2Deg;
                 
                Vector3 ang = transform.eulerAngles;
                float playerAngle = transform.eulerAngles.y;
                playerAngle = Mathf.MoveTowardsAngle(playerAngle, (rel - currentAngle), PLAYER_ROTATE_PER_SECOND * Time.deltaTime);


                ang.y = playerAngle;
                transform.eulerAngles = ang;
            }
        }


        if (transform.position.y < LevelVars.deathZ)
            WaterOrFallDeath(2);  //Death by fall should do more damage.
    }

    public static void UpdateCheckpoint()
    {
        PlayerPhysics pp = FindObjectOfType<PlayerPhysics>();
        pp.startPos = pp.transform.position;
		pp.checkpointText.gameObject.SetActive(true);
		pp.Invoke("HideCheckpoint", 1f);
    }
	
	void HideCheckpoint()
	{
		checkpointText.gameObject.SetActive(false);
	}

    public void Die()
    {
        if(!isDead)
            StartCoroutine(DieRoutine());
    }
    bool isDead = false;
    IEnumerator DieRoutine()
    {
        GameVars.lives -= 1;
        isDead = true;
        SkinnedMeshRenderer[] sk = transform.GetComponentsInChildren<SkinnedMeshRenderer>();
        for(int i = 0; i < sk.Length; i++)
        {
            sk[i].enabled = false;
        }
        PlayerSword[] swords = FindObjectsOfType<PlayerSword>();
        foreach(PlayerSword s in swords)
        {
            s.DropSword();
        }


        GameObject g = Instantiate(diePrefab, transform.position, Quaternion.identity, transform) as GameObject;
        yield return new WaitForSeconds(6f);
		
		
		if(GameVars.lives <= 0)
		{
			LoadingUI.scene = "WorldSelection";
			GameVars.lives = 9;
		}
		
		FindObjectOfType<PlayerUI>().LoadNewLevel();
        //UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
    }

    void WaterOrFallDeath(int dmg = 1)
    {
        Health h = GetComponent<Health>();
        if (h != null)
        {
            h.DamageOverride(dmg);
            rigid.enabled = false;
            transform.position = startPos;
            rigid.enabled = true;
        }

        Skateboard[] sb = FindObjectsOfType<Skateboard>();
        foreach(Skateboard s in sb)
        {
            s.ResetPos();
        }

    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.collider.attachedRigidbody != null)
        {
            Debug.Log("Touching " + hit.collider.attachedRigidbody.gameObject.name);
            hit.collider.attachedRigidbody.gameObject.SendMessage("ControlHit", new ControllerCollisionMessage(gameObject, hit), SendMessageOptions.DontRequireReceiver);
        }

        if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Water"))
        {
            WaterOrFallDeath();
        }
        
    }

    
}

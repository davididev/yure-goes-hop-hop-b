﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerCollisionMessage {

    public GameObject caster;
    public ControllerColliderHit hit;

    /// <summary>
    /// Create a new Controller Collision Message since Unity's SendMessage sysetm only allows for one
    /// </summary>
    /// <param name="g">Character Controller that casts it</param>
    /// <param name="h">The collider hit object</param>
    public ControllerCollisionMessage(GameObject g, ControllerColliderHit h)
    {
        caster = g;
        hit = h;
    }
}

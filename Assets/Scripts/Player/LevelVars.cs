﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelVars : MonoBehaviour {

    public AudioClip levelMusic;
    public int timer = 200;
    public float _deathZ = -100f;
    public static float deathZ = -50000f;
    public TextAsset levelScript;

    public const float POWERUP_TIME = 40f;


	// Use this for initialization
	void Start () {
        deathZ = _deathZ;
        PlayerUI.timer = timer;
        if (levelMusic != null)
        {
            AudioSource asrc = Camera.main.gameObject.AddComponent<AudioSource>();
            asrc.clip = levelMusic;
            asrc.minDistance = 5f;
            asrc.loop = true;
            asrc.Play();
         }

        //Start up the opening cutscene
        if(LoadingUI.repeatScene == false && levelScript != null)
        {
            FindObjectOfType<DialogueHandler>().StartEvent(levelScript);
        }
		
		LoadingUI.scene = SceneManager.GetActiveScene().name;
	}

    private void OnDestroy()
    {
        deathZ = -50000f;
    }

    // Update is called once per frame
    void Update () {
		
	}
}

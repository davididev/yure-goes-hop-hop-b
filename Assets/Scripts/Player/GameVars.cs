﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVars {

    public static int fileID = 0;
    public static int lives = 9;

    public static bool[] worldsCompleted = { false, false, false, false };
    public static int[] miscVars = new int[10];

    public static void ResetForWorld()
    {
        lives = 9;
    }

    public static void NewFile()
    {
        ResetForWorld();
        for(int i = 0; i < worldsCompleted.Length; i++)
        {
            worldsCompleted[i] = false;
        }
    }

    public static void SaveFile()
    {
        string s = "";
        for (int i = 0; i < worldsCompleted.Length; i++)
        {
            if (worldsCompleted[i] == false)
                s = s + "0";
            else
                s = s + "1";
        }
        PlayerPrefs.SetString(fileID + "worlds", s);


        s = "";
        for(int i = 0; i < miscVars.Length; i++)
        {
            s = s + i.ToString() + ",";
        }

        PlayerPrefs.SetString(fileID + "misc", s);
    }

    public static void LoadFile(int id)
    {
        string s = PlayerPrefs.GetString(id + "worlds");
        char[] wordWorlds = s.ToCharArray();
        for(int i = 0; i < wordWorlds.Length; i++)
        {
            if (wordWorlds[i] == '0')
                worldsCompleted[i] = false;
            else
                worldsCompleted[i] = true;
        }

        s = PlayerPrefs.GetString(id + "misc");
        string[] numbers = s.Split(',');
        for(int i = 0; i < miscVars.Length; i++)
        {
            if (i >= numbers.Length)  //We needed more vars than originally allocated
                miscVars[i] = 0;
            else
                miscVars[i] = int.Parse(numbers[i]);
        }
    }
}

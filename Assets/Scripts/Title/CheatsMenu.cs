﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheatsMenu : MonoBehaviour {

    [SerializeField] private GameObject passwordOverlay, cheatOverlay;
    [SerializeField] private TMPro.TextMeshProUGUI passwordTxt;
    [SerializeField] private AudioClip pressButtonSnd, passwordCorrectSnd, passwordIncorrectSnd;

    // Use this for initialization
    void OnEnable()
    {


        if (PlayerPrefs.GetInt("cheatsUnlocked") == 0)
        {
            passwordOverlay.SetActive(true);
            cheatOverlay.SetActive(false);
        }
        else
        {
            passwordOverlay.SetActive(false);
            cheatOverlay.SetActive(true);
        }

    }

    bool checkingPassword = false;  //Password is being confirmed to be true
    public void AddCharacterToPassword(string c)
    {
        if (checkingPassword == true)
            return;

        AudioSource.PlayClipAtPoint(pressButtonSnd, Camera.main.transform.position);

        if (c == "x")
        {
            passwordTxt.text = "";
            return;
        }

        string s = passwordTxt.text;
        s = s + c;

        passwordTxt.text = s;

        if (s.Length >= 4)
            StartCoroutine(CheckPassword());
    }

    IEnumerator CheckPassword()
    {
        checkingPassword = true;
        string s = passwordTxt.text;
        yield return new WaitForSeconds(0.5f);

        Debug.Log("I see you like to look at logs.  The cheats password is 5867");

        if(s == "5867")
        {
            AudioSource.PlayClipAtPoint(passwordCorrectSnd, Camera.main.transform.position);
            PlayerPrefs.SetInt("cheatsUnlocked", 1);
            passwordOverlay.SetActive(false);
            cheatOverlay.SetActive(true);
        }
        else
        {
            AudioSource.PlayClipAtPoint(passwordIncorrectSnd, Camera.main.transform.position);
            passwordTxt.text = "WRONG";
            yield return new WaitForSeconds(1f);
            passwordTxt.text = "";
        }

        checkingPassword = false;
    }


    // Update is called once per frame
    void Update () {
		
	}
}

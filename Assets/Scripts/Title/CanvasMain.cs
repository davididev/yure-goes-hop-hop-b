﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CanvasMain : MonoBehaviour {

    [SerializeField] private GameObject mainMenu, cheatMenu, fileMenu, creditMenu, AFPanel;
	
	private static bool afDisplay = false;
	
	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.None;
		
		if(afDisplay == false)
		{
			DateTime today = DateTime.Today;
			int month = today.Month;
			int day = today.Day;
			
			//month = 4; day = 1;  //For testing purposes ONLY
			
			afDisplay = true;
			if(month == 4 && day == 1)
			{
				mainMenu.SetActive(false);
				AFPanel.SetActive(true);
			}
		}
	}

    public void SetMenuID(int id)
    {
        mainMenu.SetActive(id == 0);
        cheatMenu.SetActive(id == 1);
        creditMenu.SetActive(id == 2);
        fileMenu.SetActive(id == 3);
		
		AFPanel.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }


	
	// Update is called once per frame
	void Update () {
		
	}
}

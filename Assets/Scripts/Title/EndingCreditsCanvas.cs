﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingCreditsCanvas : MonoBehaviour {

	public TextAsset creditsFile;
	public TMPro.TextMeshProUGUI creditsText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void RunCredits()
	{
		StartCoroutine(CreditsRoutine());
	}
	
	IEnumerator CreditsRoutine()
	{
		string[] lines = creditsFile.text.Split('*');
		
		for(int i = 0; i < lines.Length; i++)
		{
			creditsText.text = lines[i];
			yield return new WaitForSeconds(5f);
		}
		
		creditsText.text = "Thanks for playing!";
		yield return new WaitForSeconds(5f);
		LoadingUI.scene = "Title";
		UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
	}
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FilesMenu : MonoBehaviour {

    public GameObject fileMenuOriginal;
    private GameObject[] filePanels;
    public RectTransform fileMenuHolder;
    private const int MAX_FILES = 4;

    private bool firstRun = true;

	// Use this for initialization
	void OnEnable () {
		if(firstRun)
        {
            firstRun = false;
            
            EventSystem.current.SetSelectedGameObject(fileMenuOriginal.GetComponent<FilePanel>().playButton.gameObject);
            filePanels = new GameObject[MAX_FILES];
            filePanels[0] = fileMenuOriginal;
            for (int i = 1; i < MAX_FILES; i++)
            {
                filePanels[i] = GameObject.Instantiate(fileMenuOriginal, fileMenuHolder) as GameObject;
                //Add info here.
            }
        }


        RefreshFileData();
	}

    void RefreshFileData()
    {
        for (int i = 0; i < MAX_FILES; i++)
        {
            filePanels[i].GetComponent<FilePanel>().DrawDetails(i);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

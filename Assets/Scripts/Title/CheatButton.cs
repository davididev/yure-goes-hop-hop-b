﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheatButton : MonoBehaviour {

    [SerializeField] private TMPro.TextMeshProUGUI textOverlay;

    public string powerupDisplay, powerupVar;

	// Use this for initialization
	void OnEnable () {
        UpdateText();

    }

    bool GetIsEnabled()
    {
        return PlayerPrefs.GetInt("Cheat" + powerupVar) == 1;
    }

    public void ToggleCheat()
    {
        bool b = !GetIsEnabled();

        if (b == true)
            PlayerPrefs.SetInt("Cheat" + powerupVar, 1);
        else
            PlayerPrefs.SetInt("Cheat" + powerupVar, 0);
        UpdateText();
    }

    void UpdateText()
    {
        textOverlay.text = powerupDisplay + "\n" + (GetIsEnabled() ? "On" : "Off");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

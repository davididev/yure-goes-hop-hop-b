﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsMenu : MonoBehaviour {

    [SerializeField] private TMPro.TextMeshProUGUI mainInfoText;
    [SerializeField] private TextAsset creditsFile;

    // Use this for initialization
    void OnEnable()
    {
        StartCoroutine(Routine());
    }

    IEnumerator Routine()
    {
        string[] lines = creditsFile.text.Split('*');
        int i = 0;
        while(gameObject.activeSelf)
        {
            mainInfoText.text = lines[i];
            yield return new WaitForSeconds(5f);
            i++;
            if (i >= lines.Length)
                i = 0;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AFPanel : MonoBehaviour {

	public GameObject[] panels;

	private int currentPanel = -1;
	
	
	public void Continue()
	{
		currentPanel++;
		
		for(int i = 0; i < panels.Length; i++)
		{
			panels[i].SetActive(i == currentPanel);
		}
	}
	
	// Use this for initialization
	void OnEnable () {
		Continue();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

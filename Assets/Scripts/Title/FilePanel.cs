﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FilePanel : MonoBehaviour {

    [SerializeField] private TMPro.TextMeshProUGUI fileDetails;
    [SerializeField] public Button playButton, deleteButton;
	public Animator bunnyAnim;


    private int fileID = 0;
    private bool newFile = false;

	// Use this for initialization
	void Start () {
		
	}

    public void DrawDetails(int id)
    {
        fileID = id;

        string s = "File " + (fileID + 1) + ": \n";

        GlobalData.fileID = fileID;
        bool success =  GlobalData.instance.LoadFile();
        
        if(success == false)
        {
            s = s + "New File";
            deleteButton.interactable = false;
            newFile = true;
        }
        else
        {
            deleteButton.interactable = true;
            bool[] w = GlobalData.instance.worldsCompleted;
            int worlds = 0;
            for(int i = 0; i < w.Length; i++)
            {
                if (w[i] == true)
                    worlds++;
            }

            s = s + "Worlds Done: " + worlds;
            newFile = false;
        }


        fileDetails.text = s;

    }

    public void DeleteFile()
    {
        File.Delete(Application.persistentDataPath + "/file" + fileID);
        EventSystem.current.SetSelectedGameObject(playButton.gameObject);
        DrawDetails(fileID);
    }

    public void PlayGame()
    {
        StartCoroutine(PlayGameRoutine());

    }
	
	public IEnumerator PlayGameRoutine()
	{
		yield return new WaitForSeconds(0.5f);
		bunnyAnim.SetTrigger("End Level");
		yield return new WaitForSeconds(1f);
		GlobalData.fileID = fileID;
        if (GlobalData.instance.LoadFile() == true)
        {
            LoadingUI.scene = "WorldSelection";
            UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
        }
        else
        {
            GlobalData.instance.NewFile();
            LoadingUI.scene = "Intro";
            UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour {

    public static string scene = "TestScene";
    private static string lastScene = "";
    public static bool repeatScene { get; private set; } 

	
    [SerializeField] private Image loadingBar;

	// Use this for initialization
	void Start () {
        repeatScene = (lastScene == scene);
        lastScene = scene;

        StartCoroutine(LoadingRoutine());
        
	}
	
	

    IEnumerator LoadingRoutine()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;
        AsyncOperation load = SceneManager.LoadSceneAsync(scene);
        float perc = 0f;
        while(load.isDone == false)
        {
            perc = load.progress;
            loadingBar.fillAmount = perc;
            loadingBar.color = Color.Lerp(new Color(0.5f, 0f, 0f), Color.green, perc);
            yield return new WaitForEndOfFrame();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WorldSelectionUI : MonoBehaviour {


    public TMPro.TMP_Text worldText;
    public Image checkOrMinus;
    public Sprite check, minus;
    private int worldNumber = 1;
    const float MOVE_SPEED = 60f;

	public Animator kittyAnim;
	
	// Use this for initialization
	void Start () {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;

        GlobalData.instance.SaveFile();
        ChangeWorldNumber(1);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 currentPos = Camera.main.transform.position;
        Vector3 targetPos = currentPos;
        targetPos.x = (worldNumber - 1) * 40f;

        Camera.main.transform.position = Vector3.MoveTowards(currentPos, targetPos, MOVE_SPEED * Time.deltaTime);
	}

    public void IncrementWorldNumber()
    {
        int i = worldNumber + 1;
        if (i > 4)
            i = 4;
        ChangeWorldNumber(i);
    }

    public void DecreaseWorldNumber()
    {
        int i = worldNumber - 1;
        if (i < 1)
            i = 1;
        ChangeWorldNumber(i);
    }

    public void PlayWorld()
    {
        if(worldNumber == 4)
        {
            //TODO: Check for completed worlds first
			//Note: this will now be accomplished via dialogue system.
            //return;  //Return should only be if World 4 is NOT available.
        }

        LoadingUI.scene = "W" + worldNumber + "L1";
        StartCoroutine(GoToLoadingScene());
    }

    IEnumerator GoToLoadingScene()
    {
        yield return new WaitForSeconds(0.5f);
		kittyAnim.SetTrigger("End Level");
		yield return new WaitForSecondsNoTimeScale(1f);
		
        SceneManager.LoadScene("LoadingScene");
    }

    public void Quit()
    {
        SceneManager.LoadScene("Title");
        //Later this should point to the title screen.
    }

    private void ChangeWorldNumber(int i)
    {
        worldText.text = "World " + i;

        checkOrMinus.sprite = (GlobalData.instance.worldsCompleted[i - 1] == true) ? check : minus;
        worldNumber = i;
        Vector3 newCameraVector = new Vector3((i - 1) * 40f, 10f, -34f);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinnerUI : MonoBehaviour {

    public static int keyNum = 1;
    public TMPro.TextMeshProUGUI txt;

	public Animator bunny;
	
    public Transform possibleKeys;

    // Use this for initialization
    void Start()
    {
		Cursor.lockState = CursorLockMode.None;
		Time.timeScale = 1f;
        string s = txt.text;
        s = s.Replace("<x>", keyNum.ToString());

		txt.text = s;
		
		
        Transform[] children = new Transform[possibleKeys.childCount];
        for(int i = 0; i < children.Length; i++)
        {
            children[i] = possibleKeys.GetChild(i);
            children[i].gameObject.SetActive((keyNum - 1) == i);
        }

    }

    public void ConfirmChanges()
    {
        StartCoroutine(PressButton());
    }
	
	IEnumerator PressButton()
	{
		yield return new WaitForSeconds(0.5f);
		GlobalData.instance.worldsCompleted[keyNum - 1] = true;
        LoadingUI.scene = "WorldSelection";
        bunny.SetTrigger("End Level");
		
		yield return new WaitForSeconds(1f);
		SceneManager.LoadScene("LoadingScene");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

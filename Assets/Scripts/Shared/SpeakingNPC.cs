﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeakingNPC : MonoBehaviour {

    public TextAsset dialogue, startupDialogue;
    private float talkTime = 0f;
    private float targetRot = -1f;

	// Use this for initialization
	void Start () {
		if(startupDialogue != null)
			FindObjectOfType<DialogueHandler>().StartEvent(startupDialogue);
	}
	
	// Update is called once per frame
	void Update () {
        if (targetRot != -1f)
        {
            Vector3 ang = transform.eulerAngles;
            ang.y = Mathf.MoveTowardsAngle(ang.y, targetRot, 360 * Time.deltaTime);
            transform.eulerAngles = ang;

            if (DialogueHandler.IS_RUNNING)
                return;
            if (talkTime > 0f)
                talkTime -= Time.deltaTime; 

        }
		
	}

    public bool SpeakToNPC(Transform src)
    {
        if (talkTime > 0f)
            return false;

        targetRot = Mathf.Atan2(src.transform.position.x - transform.position.x, src.transform.position.z - transform.position.z) * Mathf.Rad2Deg;

        talkTime = 1f;
        FindObjectOfType<DialogueHandler>().StartEvent(dialogue);

        return true;
    }
}

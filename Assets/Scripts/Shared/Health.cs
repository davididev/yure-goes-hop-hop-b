﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    [SerializeField] private int maxHealth = 6;
    private int currentHealth;
    [SerializeField] private AudioClip damageSound;

    [SerializeField] private float damageDelay = 1f;

    [SerializeField] private bool isPlayer = false;

    public static int playerHealth;
    [SerializeField] private UnityEvent onHit, onHitWhileInvincible;
    [SerializeField] private UnityEvent onDie;

    [HideInInspector] public Transform lastDamager;

    public bool invincible = false, waterKills = true;

    public float healthPerc
    {
        get
        {
            return (currentHealth * 100 / maxHealth) / 100f;
        }
    }

	// Use this for initialization
	void OnEnable () {
        lastDamager = null;
        currentHealth = maxHealth;
        if (isPlayer)
        {
            playerHealth = currentHealth;
        }
    }

    private float damageTimer = 0f;
    // Update is called once per frame
    void Update () {
        if (isPlayer)
            playerHealth = currentHealth;


        if (damageTimer > 0f)
            damageTimer -= Time.deltaTime;
    }

    /// <summary>
    /// Only damage if target is done recovering from last hit.
    /// </summary>
    /// <param name="amt"></param>
    public bool Damage(int amt, Transform damager = null)
    {
        if (invincible)
        {
            onHitWhileInvincible.Invoke();
            return false;
        }
        if (damageTimer <= 0f)
        {
            damageTimer = damageDelay;
            DamageOverride(amt, damager);
            return true;
        }

        return false;
        
    }

    /// <summary>
    /// Damage regardless of timer
    /// </summary>
    /// <param name="amt"></param>
    public void DamageOverride(int amt, Transform damager = null)
    {
        if (invincible)
            return;
        currentHealth -= amt;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        if (currentHealth <= 0)
        {
            
            onDie.Invoke();
        }
        else
        {
            if (amt < 0)
                return;
            if (damageSound != null)
                AudioSource.PlayClipAtPoint(damageSound, transform.position);
            onHit.Invoke();
            lastDamager = damager;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (waterKills)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Water"))
            {
                currentHealth = 0;
                onDie.Invoke();
            }
        }
    }

	
	private void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (waterKills)
        {
            if (hit.gameObject.layer == LayerMask.NameToLayer("Water"))
            {
                currentHealth = 0;
                onDie.Invoke();
            }
        }
	}

}

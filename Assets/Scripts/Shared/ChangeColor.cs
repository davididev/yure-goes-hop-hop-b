﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour {

    [SerializeField] Color changeToThis = Color.red;
    [SerializeField] float waitTimer = 0.25f;
    [SerializeField] Renderer[] rendsToChange;

    Color[] startingColor;

	// Use this for initialization
	void Start () {
        startingColor = new Color[rendsToChange.Length];
        for(int i = 0; i < startingColor.Length; i++)
        {
            startingColor[i] = rendsToChange[i].material.color;
        }
	}

    public void StartFlash()
    {
        StartCoroutine(ColorFlash());
    }

    IEnumerator ColorFlash()
    {
        for(int i = 0; i < rendsToChange.Length; i++)
        {
            rendsToChange[i].material.color = changeToThis;
        }
        yield return new WaitForSeconds(waitTimer);

        for (int i = 0; i < rendsToChange.Length; i++)
        {
            rendsToChange[i].material.color = startingColor[i];
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}

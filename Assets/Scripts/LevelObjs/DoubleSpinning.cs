﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleSpinning : MonoBehaviour {

    private Animator anim;
    public RotatePerSecond[] platforms;

    public float spinningMultiplier = 1f, rotatePerSecond = 120f;
    public bool mirror = false;

    private float _lastRotatePerSecond = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (anim == null)
            anim = GetComponent<Animator>();

        anim.SetFloat("TimeScale", spinningMultiplier);
        anim.SetBool("Mirror", mirror);

        if (!Mathf.Approximately(_lastRotatePerSecond, rotatePerSecond))
        {
            _lastRotatePerSecond = rotatePerSecond;
            foreach (RotatePerSecond rps in platforms)
            {
                Vector3 v = rps.rotatePerSecond;
                v.y = rotatePerSecond;
                rps.rotatePerSecond = v;
            }
        }
	}
}

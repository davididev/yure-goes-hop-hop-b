﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScifiKey : MonoBehaviour {
    public enum KEY_ID { RED=0, GREEN=1, BLUE=2, NIL=3};
    public KEY_ID whichKey = KEY_ID.RED;

    public AudioClip pickupSnd;

    public static bool[] keysAssembled = { false, false, false };
	// Use this for initialization
	void Start () {
		
	}

    private void OnDestroy()
    {
        //Reset on level unload.
        for(int i = 0; i < keysAssembled.Length; i++)
        {
            keysAssembled[i] = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(whichKey != KEY_ID.NIL)
            {
                keysAssembled[(int)whichKey] = true;
                if (pickupSnd != null)
                    AudioSource.PlayClipAtPoint(pickupSnd, transform.position);
                gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}

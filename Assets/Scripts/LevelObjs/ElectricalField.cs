﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ElectricalField : MonoBehaviour {

    [SerializeField] public float xDist = 10f, zDist = 5f, anglesPerSecond = 35f;
    public int touchDamage = 1;
    [SerializeField] public bool alternateDirection = false;
    private SphereCollider col;
    [SerializeField] private AudioClip electricSoundFX;

    private Vector3 startingPos;
    private float currentAngle = 0f;
    
    private Rigidbody rigid;

	// Use this for initialization
	void Start () {
        startingPos = transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (col == null)
            col = GetComponent<SphereCollider>();

        if (alternateDirection)
            currentAngle -= anglesPerSecond * Time.deltaTime;
        else
            currentAngle += anglesPerSecond * Time.deltaTime;
        if (currentAngle > 360f)
            currentAngle -= 360f;
        if (currentAngle < 0f)
            currentAngle += 360f;

        rigid.position = startingPos + new Vector3(Mathf.Cos(currentAngle * Mathf.Deg2Rad) * xDist, 0f, Mathf.Sin(currentAngle * Mathf.Deg2Rad) * zDist);


        Collider[] cols = PhysicsExtensions.OverlapSphere(col, LayerMask.GetMask("Player"));
        if(cols.Length > 0)
        {
            foreach(Collider c in cols)
            {
                Health h = c.GetComponent<Health>();
                if(h.Damage(touchDamage) == true)
                    AudioSource.PlayClipAtPoint(electricSoundFX, rigid.position);
            }

            
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScifiDoor : MonoBehaviour {

    private const float SCAN_RANGE = 3f;
    public ScifiKey.KEY_ID typeOfDoor = ScifiKey.KEY_ID.NIL;

    public LayerMask thingsToAllow;

    [SerializeField] private SpriteRenderer[] rends;
    private Animator anim;
    [SerializeField] AudioClip openSnd, closeSnd;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();


        if (typeOfDoor == ScifiKey.KEY_ID.RED)
            SetDoorColor(Color.red);
        if (typeOfDoor == ScifiKey.KEY_ID.GREEN)
            SetDoorColor(Color.green);
        if (typeOfDoor == ScifiKey.KEY_ID.BLUE)
            SetDoorColor(Color.blue);
    }

    void SetDoorColor(Color c)
    {
        for(int i = 0; i < rends.Length; i++)
        {
            rends[i].material.SetColor("_Color", c);
        }
    }

    private Coroutine openRoutine;
	// Update is called once per frame
	void Update () {
        if(anim == null)
            anim = GetComponent<Animator>();
        
        if (doorOpened == false)
        {
            Collider[] c = Physics.OverlapSphere(transform.position, SCAN_RANGE, thingsToAllow);
            if (c.Length != 0 && IsOpenable())
            {
                openRoutine = StartCoroutine(OpenSesame());
            }
        }

	}

    IEnumerator OpenSesame()
    {
        doorOpened = true;
        AudioSource.PlayClipAtPoint(openSnd, transform.position);
        anim.SetTrigger("Open");
        yield return new WaitForSeconds(5f);
        bool canClose = false;
        while(!canClose)
        {
            Collider[] c = Physics.OverlapSphere(transform.position, SCAN_RANGE, thingsToAllow);
            if (c.Length == 0)
                canClose = true;

            yield return new WaitForEndOfFrame();
        }

        anim.SetTrigger("Close");
        AudioSource.PlayClipAtPoint(closeSnd, transform.position);
        yield return new WaitForSeconds(0.2f);
        doorOpened = false;
    }

    bool doorOpened = false;

    private bool IsOpenable()
    {
        if (typeOfDoor == ScifiKey.KEY_ID.NIL)
            return true;

        int index = (int)typeOfDoor;
        if (ScifiKey.keysAssembled[index] == true)
            return true;

        return false;
    }
}

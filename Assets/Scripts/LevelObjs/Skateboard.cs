﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skateboard : MonoBehaviour {

    [SerializeField] private AudioClip skatingMusic;
	[SerializeField] private ParticleSystem splashers;
    private Rigidbody rigid;
    private GameObject playerGrip;

    private const float MOVE_SPEED = 50f, MAX_SPEED = 500f;

	// Use this for initialization
	void Start () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        startingPos = rigid.position;
        lastPos = rigid.position;
    }

    public void ResetPos()
    {
		Quaternion lol = rigid.rotation;
		lol.eulerAngles = Vector3.zero;
		rigid.rotation = lol; 
        rigid.position = startingPos;
    }

    private Vector3 startingPos;
    private Vector3 lastPos;
	// Update is called once per frame
	void FixedUpdate () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();

        Vector3 difference = rigid.position - lastPos;

        lastPos = rigid.position;
        if (playerGrip != null)
        {
			
			if(isInWater == true && splashers.isPlaying == false)
				splashers.Play();
			
            Debug.Log("GO GO GO");
            Vector3 pos = playerGrip.transform.position;
            pos = pos + difference;
            playerGrip.transform.position = pos;
            rigid.AddForce(Vector3.up * 1.5f, ForceMode.Acceleration);
            if(rigid.velocity.sqrMagnitude <= MAX_SPEED)
                rigid.AddForce(playerGrip.transform.forward * MOVE_SPEED, ForceMode.Impulse);


            float dist = Vector3.Distance(playerGrip.transform.position, transform.position);
            Debug.Log("Dist: " + dist);
            if (dist > 2.5f)
            {
                playerGrip = null;
                AudioSource src = Camera.main.GetComponent<AudioSource>();
                src.clip = FindObjectOfType<LevelVars>().levelMusic;
                src.Play();
            }
        }
		else  //Player is not on the skateboard
		{
			splashers.Stop();
		}

        
    }


    void ControlHit(ControllerCollisionMessage hit)
    {
        Debug.Log("Normal with " + gameObject.name + ": " + hit.hit.normal.ToString());
        if (hit.hit.normal.y < 0.8)
            return;

        if (playerGrip == null)
        {
            playerGrip = hit.caster;
            AudioSource src = Camera.main.GetComponent<AudioSource>();
            src.clip = skatingMusic;
            src.Play();
        }
    }

    /*
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Normal with " + gameObject.name + ": " + collision.contacts[0].normal.ToString());

        if (collision.contacts[0].normal.y > -0.8)
            return;
        if(collision.gameObject.tag == "Player")
        {
            playerGrip = collision.gameObject;
            AudioSource src = Camera.main.GetComponent<AudioSource>();
            src.clip = skatingMusic;
            src.Play();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("But mooom, " + collision.gameObject.name + " left me!");
        if (collision.gameObject.tag == "Player")
        {
            playerGrip = null;

            AudioSource src = Camera.main.GetComponent<AudioSource>();
            src.clip = FindObjectOfType<LevelVars>().levelMusic;
            src.Play();
        }
    }
    */
	
	bool isInWater = false;
	
	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("Water"))
		{
			isInWater = true;
		}
	}
	private void OnCollisionExit(Collision collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("Water"))
		{
			isInWater = false;
			splashers.Stop();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalSign : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 ang = transform.eulerAngles;
        ang.y += 180f * Time.deltaTime;
        transform.eulerAngles = ang;
	}

    void OnTriggerEnter(Collider hit)
    {
        if(hit.gameObject.tag == "Player")
        {
            string s = SceneManager.GetActiveScene().name;
            char currentNum = s[3];
            int x = int.Parse(currentNum.ToString());
            x++;
            LoadingUI.scene = s.Substring(0, 3) + x;
            
			FindObjectOfType<PlayerUI>().LoadNewLevel();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

    [SerializeField] private AudioClip powerupClip;
    private const float ADDITIONAL_TIME = 60f + 25f;  //Time that passes after the powerup expires before it re-appears.
    public enum MY_TYPE { DoubleJump, Fireball, Invincibility, Heart }
    public MY_TYPE type = MY_TYPE.DoubleJump;

    private GameObject rendPiece;
    

	// Use this for initialization
	void Awake () {
        if (rendPiece == null)
            rendPiece = transform.GetChild(0).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if (rendPiece == null)
            rendPiece = transform.GetChild(0).gameObject;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (rendPiece.activeInHierarchy == false)  //Invisible- don't use powerups atm
            return;

        if(other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(powerupClip, transform.position);

            if(type == MY_TYPE.Heart)
            {
                Health h = other.GetComponent<Health>();
                h.DamageOverride(-2);
                StartCoroutine(MakeInviz());
            }

            if (type == MY_TYPE.DoubleJump)
            {
                PlayerPhysics ph = other.GetComponent<PlayerPhysics>();
                ph.StartDoubleJump();
                StartCoroutine(MakeInviz());
            }
            if (type == MY_TYPE.Fireball)
            {
                PlayerSword[] ps = other.GetComponentsInChildren<PlayerSword>();
                foreach(PlayerSword p in ps)
                {
                    p.StartFireballRoutine();
                }

                StartCoroutine(MakeInviz());
            }

            if (type == MY_TYPE.Invincibility)
            {
                PlayerPhysics ph = other.GetComponent<PlayerPhysics>();
                ph.BecomeInvincible();
                StartCoroutine(MakeInviz());
            }
        }
    }

    IEnumerator MakeInviz()
    {
        rendPiece.SetActive(false);
        yield return new WaitForSeconds(LevelVars.POWERUP_TIME);
        yield return new WaitForSeconds(ADDITIONAL_TIME);
        rendPiece.SetActive(true);
    }
}

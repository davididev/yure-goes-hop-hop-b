﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalData {

    public static GlobalData instance = new GlobalData();

    public static int fileID = 0;

    public bool[] worldsCompleted = { false, false, false, false };
    public int easterEggProgress = 0;

    public void NewFile()
    {
        for(int i = 0; i < worldsCompleted.Length; i++)
        {
            worldsCompleted[i] = false;
        }
        easterEggProgress = 0;

        SaveFile();

    }


    /// <summary>
    /// Convert a list of bools to a string of 0's and 1's.
    /// </summary>
    /// <param name="si"></param>
    /// <returns></returns>
    string BoolToString(bool[] si)
    {
        string s = "B";  //Needs to be padded with one string value or else the CSV writer will not read it as a string
        for(int i = 0; i < si.Length; i++)
        {
            if (si[i] == true)
                s = s + '1';
            if (si[i] == false)
                s = s + '0';
        }

        return s;
    }
    
    /// <summary>
    /// Converts a string of 0's and 1's to a bool array
    /// </summary>
    /// <param name="s">String of 0/1</param>
    /// <param name="size">Number of items in the string.</param>
    /// <returns></returns>
    bool[] StringToBool(string s, int size)
    {
        bool[] list = new bool[size];
        for(int i = 1; i < s.Length; i++)
        {
            if (s[i] == '0')
                list[i-1] = false;
            if (s[i] == '1')
                list[i-1] = true;
        }
        return list;
    }

    public void SaveFile()
    {
        CSVWriter.Reset();
        CSVWriter.WriteLine(new object[] { "@Worlds", BoolToString(worldsCompleted) });
        CSVWriter.WriteLine(new object[] { "@Egg", easterEggProgress });
        CSVWriter.SaveFile(Application.persistentDataPath + "/file" + fileID);
    }

    /// <summary>
    /// Load the file; 
    /// </summary>
    /// <returns>False if file does not exist; true if success</returns>
    public bool LoadFile()
    {
        string[] lines = CSVReader.GetFile(Application.persistentDataPath + "/file" + fileID);

        if(lines == null)
        {
            return false;
        }

        for(int i = 0; i < lines.Length; i++)
        {
            object[] obj = CSVReader.GetArgumentsInLine(lines[i]);

            if((string) obj[0] == "@Worlds")
            {
                worldsCompleted = StringToBool((string)obj[1], worldsCompleted.Length);
            }
            if((string) obj[0] == "@Egg")
            {
                easterEggProgress = (int) obj[1];
            }
        }

        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Enemy {

    public GameObject slimeExplodePrefab;
    private GameObject[] explodeInstances;
    [SerializeField] private Animator anim;

    protected override void OnSpawn()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        if(explodeInstances == null)
        {
            explodeInstances = new GameObject[4];
            for(int i = 0; i < explodeInstances.Length; i++)
            {
                explodeInstances[i] = GameObject.Instantiate(slimeExplodePrefab) as GameObject;
                explodeInstances[i].SetActive(false);
            }
        }
    }

    void UseExplosion()
    {
        for(int i = 0; i < explodeInstances.Length; i++)
        {
            if(explodeInstances[i].activeSelf == false)
            {
                //We'll use this one!
                explodeInstances[i].SetActive(true);
                explodeInstances[i].transform.position = transform.position;

                break;
            }
        }
    }

    public void CreateParticleBurst()
    {
        UseExplosion();

        float p = this.health.healthPerc;
        transform.localScale = new Vector3(p, p, p);

    }


    protected override IEnumerator MainFunction()
    {
        this.currentBrain = BRAIN.StandStillAndLookForTarget;
        anim.SetFloat("moveSpeed", 0f);
        while (target == null)
        {
            if (health.lastDamager != null)
            {
                Debug.Log("LOL " + health.lastDamager.gameObject.name);
                target = health.lastDamager;
            }
            yield return new WaitForEndOfFrame();
        }

        this.currentBrain = BRAIN.FollowTarget;

        anim.SetFloat("moveSpeed", 1f);
        while (true)
        {
            anim.SetFloat("movePerc", (rigid.velocity.sqrMagnitude / maxMoveSpeed));
            yield return new WaitForEndOfFrame();
        }
    }
}

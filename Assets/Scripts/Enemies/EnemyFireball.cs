﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireball : MonoBehaviour {

    private Rigidbody rigid;
    private const float MOVE_SPEED = 10f;
    private const float TIMER = 2.0f;
    private float time = 0f;
    // Use this for initialization
    void OnEnable()
    {
        time = TIMER;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();

        rigid.velocity = MOVE_SPEED * transform.forward;
        time -= Time.fixedDeltaTime;
        if (time < 0f)
            gameObject.SetActive(false);
    }



    void ControlHit(ControllerCollisionMessage hit)
    {
        Health h = hit.caster.GetComponent<Health>();
        if (h != null)
        {
            h.Damage(1);
            gameObject.SetActive(false);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Health h = collision.transform.GetComponent<Health>();
            if (h != null)
            {
                h.Damage(1);
                gameObject.SetActive(false);
            }
        }
    }
}

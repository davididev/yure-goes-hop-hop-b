﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    protected Rigidbody rigid;
    protected Health health;
    protected Transform target;
    private Coroutine enemyRoutine;

    protected enum BRAIN { Custom, StandStillAndLookForTarget, StandStillAndWaitForTarget, WanderAroundAndLookForTarget, FollowTarget}
    [SerializeField] protected BRAIN currentBrain;
    public LayerMask targetMask;

    [SerializeField] protected float rotateSpeed, moveSpeed, maxMoveSpeed, slowDownSpeed;
    [SerializeField] protected int touchDamage;
    protected Vector3 localMoveVec;  //Sidestep / foward basically

	// Use this for initialization
	void OnEnable () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (health == null)
            health = GetComponent<Health>();

        enemyRoutine = StartCoroutine(MainFunction());
        OnSpawn();
	}

    private void OnDisable()
    {
        if(enemyRoutine != null)
			StopCoroutine(enemyRoutine);
        //OnDeath();
    }

    // Update is called once per frame
    void Update () {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (health == null)
            health = GetComponent<Health>();


        //Run brains- be sure to ignore custom.
        if (currentBrain == BRAIN.StandStillAndWaitForTarget)
        {
            //Randomize turning
            ScanForTargets();
            localMoveVec = Vector3.zero;
        }

        if (currentBrain == BRAIN.StandStillAndLookForTarget)
        {
            //Randomize turning
            PickRandomRotation();
            ScanForTargets();
            localMoveVec = Vector3.zero;
        }

        if(currentBrain == BRAIN.WanderAroundAndLookForTarget)
        {
            //Randomize turning and move forward sometimes.
            PickRandomRotation();
            ScanForTargets();
            localMoveVec.z = 1f;
        }

        if(currentBrain == BRAIN.FollowTarget)
        {
            
            localMoveVec.z = 1f;
            Vector3 rot = transform.eulerAngles;
            float targetRot = Quaternion.LookRotation(target.position - transform.position).eulerAngles.y;
            rot.y = Mathf.MoveTowardsAngle(rot.y, targetRot, rotateSpeed * Time.deltaTime);
            transform.eulerAngles = rot;
        }

        OnUpdate();
	}

    public void ScanForTargets()
    {
        RaycastHit info;
        if(Physics.Raycast(new Ray(rigid.position, transform.forward), out info, 7.5f, this.targetMask))
        {
            target = info.transform;
        }
    }

    public void StandardDeath()
    {
        gameObject.SetActive(false);
        //Death effect we'll do later.
    }

    private void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        UseMoveVecs();
        OnFixedUpdate();
        
    }

    /// <summary>
    /// Move forward / backwards / sidesteps etc.
    /// </summary>
    void UseMoveVecs()
    {
        float vel = rigid.velocity.sqrMagnitude;
        if(vel < maxMoveSpeed)
        {
            rigid.AddRelativeForce(localMoveVec * moveSpeed, ForceMode.VelocityChange);  //Move relative to the move vector.
            
        }

        
        if(localMoveVec == Vector3.zero)  //Idle- let's slow the rigidbody down.
        {
            Vector3 velocity = rigid.velocity;
            velocity = Vector3.MoveTowards(velocity, new Vector3(0f, velocity.y, 0f), slowDownSpeed * Time.deltaTime);
            rigid.velocity = velocity;
        }
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (touchDamage > 0)
        {
            if (targetMask == (targetMask | (1 << col.gameObject.layer)))
            {
                Health h = col.gameObject.GetComponent<Health>();
                if (h != null)
                {
					if(touchDamage > 0)
						h.Damage(touchDamage);
                }
            }
        }
        EnterCol(col);
    }

    void ControlHit(ControllerCollisionMessage hit)
    {
        if (targetMask == (targetMask | (1 << hit.caster.layer)))
        {
            Health h = hit.caster.GetComponent<Health>();
            if (h != null)
            {
				if(touchDamage > 0)
					h.Damage(touchDamage);
            }
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        ExitCol(collision);
    }

    private float randomRotationAngle = 0f;
    /// <summary>
    /// Turn towards rotation and pick a random rotation if you're already there.
    /// </summary>
    void PickRandomRotation()
    {
        Vector3 rot = transform.eulerAngles;
        if (Mathf.Approximately(rot.y, randomRotationAngle))
        {
            //We're at the correct angle, let's pick a new one.
            //(use Mathf.Approximately for floats since it's nearly impossible to get it exact)
            randomRotationAngle = Random.Range(0, 360/15) * 15f;
        }
        
        rot.y = Mathf.MoveTowardsAngle(rot.y, randomRotationAngle, rotateSpeed * Time.deltaTime);
        transform.eulerAngles = rot;
        
    }
    
    /// <summary>
    /// Any custom actions that happens when the creature is created/respawned.
    /// </summary>
    protected virtual void OnSpawn()
    {

    }

    /// <summary>
    /// Any custom actions that happen when the created is killed
    /// </summary>
    public virtual void OnDeath()
    {

    }

    /// <summary>
    /// Any custom actions for when an enemy touches something else.
    /// </summary>
    /// <param name="col"></param>
    protected virtual void EnterCol(Collision col)
    {

    }

    /// <summary>
    /// Any custom actions for when an enemy is done touching.
    /// </summary>
    /// <param name="col"></param>
    protected virtual void ExitCol(Collision col)
    {

    }

    /// <summary>
    /// Custom actions to be carried every step.
    /// </summary>
    protected virtual void OnUpdate()
    {

    }

    /// <summary>
    /// Any constant physics calculations needed.
    /// </summary>
    protected virtual void OnFixedUpdate()
    {

    }

    /// <summary>
    /// Main script for an enemy.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator MainFunction()
    {
        yield return null;
    }
}

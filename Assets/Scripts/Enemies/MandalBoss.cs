﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MandalBoss : Enemy {

    public static bool activated = false;
    public int fireballsToFire = 1;
    public float delayBetweenFireballs = 5f;

    public GameObject fireballPrefab, deathPrefab;
    private GameObject[] pool;
    public TextAsset dialogueOnWin;
    private const int SIZE_OF_POOL = 10;  //How many fireballs can exist at once.
    
    public void ActivateMandal()
    {
        activated = true;
        PlayerPhysics.UpdateCheckpoint();
    }

    protected override void OnSpawn()
    {
        base.OnSpawn();
        if (pool == null)
        {
            pool = new GameObject[SIZE_OF_POOL];
            for(int i = 0; i < SIZE_OF_POOL; i++)
            {
                pool[i] = Instantiate(fireballPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                pool[i].SetActive(false);
            }
        }
        
    }

    private void SpawnAFireball(float angle)
    {
        Vector3 pos = target.position + (Vector3.up * 3.75f);
        pos.x += Mathf.Cos(angle) * 0.75f;
        pos.z += Mathf.Sin(angle) * 0.75f;

        for(int i = 0; i < pool.Length; i++)
        {
            if (pool[i].activeInHierarchy == false)
            {
                pool[i].SetActive(true);
                pool[i].transform.position = pos;
                break;
            }
        }
    }

    protected override IEnumerator MainFunction()
    {
        activated = false;
        target = GameObject.FindWithTag("Player").transform;
        while(activated == false)
        {
            yield return new WaitForEndOfFrame();
        }

        Animator anim = GetComponent<Animator>();
        int seed = 0;  //We're not doing REAL random stuff.  Player needs to eventually be able to predict the pattern.  :D
        while(gameObject)
        {
            yield return new WaitForSeconds(delayBetweenFireballs);
            anim.SetTrigger("Attack");
            for(int i = 0; i < fireballsToFire; i++)
            {
                Random.InitState(seed * i);
                SpawnAFireball(Random.Range(0, 24) * 15f);
            }
            seed++;
            
        }
    }

    public void OnAttacked()
    {
        rigid.AddRelativeForce(Vector3.forward * -2.5f, ForceMode.Impulse);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        GameObject g = GameObject.Instantiate(deathPrefab, transform.position, Quaternion.identity) as GameObject;
         
        StandardDeath();
        FindObjectOfType<DialogueHandler>().StartEvent(dialogueOnWin);
    }
}

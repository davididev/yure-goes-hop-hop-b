﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hopper : Enemy  {

    public AudioClip deathClip;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject deathPrefab;
    private GameObject deathSpawn;

    protected override void OnSpawn()
    {
        if (deathSpawn == null)
            deathSpawn = GameObject.Instantiate(deathPrefab, transform.parent) as GameObject;

        deathSpawn.SetActive(false);
    }

    protected override IEnumerator MainFunction()
    {
        float hopTimer = 0f;
        while (gameObject.activeInHierarchy)
        {
            this.localMoveVec.z = 0f;
            if (target == null)
                ScanForTargets();
            else
            {
                this.localMoveVec.z = 1f;
                Vector3 ang = transform.eulerAngles;
                float targetRot = Quaternion.LookRotation(target.position - transform.position).eulerAngles.y;
                ang.y = targetRot;
                transform.eulerAngles = ang;
            }
            if (hopTimer > 0f)
            {
                hopTimer -= Time.deltaTime;
            }
            else
            {
                anim.SetTrigger("Hop");
                rigid.AddForce(Vector3.up * 5f, ForceMode.Impulse);
                hopTimer = 1f;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public override void OnDeath()
    {
        AudioSource.PlayClipAtPoint(deathClip, transform.position);
        deathSpawn.SetActive(true);
        deathSpawn.transform.position = transform.position;

    }
}

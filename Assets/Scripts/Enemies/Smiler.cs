﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smiler : Enemy {

    public GameObject deathPrefab, bulletPrefab;
    private GameObject deathInstance;
    private GameObject[] bullets;

    protected override void OnSpawn()
    {
        if(deathInstance == null)
        {
            deathInstance = GameObject.Instantiate(deathPrefab, transform.position, Quaternion.identity) as GameObject;
            deathInstance.SetActive(false);
            bullets = new GameObject[5];
            for(int i = 0; i < bullets.Length; i++)
            {
                bullets[i] = GameObject.Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                bullets[i].SetActive(false);
            }
        }
        base.OnSpawn();
    }

    void Fire()
    {
        //Fire a bullet
        Debug.Log("Pew pew pew");
        for(int i = 0; i < bullets.Length; i++)
        {
            if(bullets[i].activeInHierarchy == false)
            {
                bullets[i].transform.position = transform.position;
                bullets[i].transform.forward = transform.forward;
                bullets[i].SetActive(true);
                break;
            }
        }
    }

    protected override IEnumerator MainFunction()
    {
        float scanTimer = 0f;
        float TIME_BETWEEN_FIRES = 0.25f;
        int layerMask = LayerMask.GetMask("Default", "Player");

        while(gameObject.activeInHierarchy)
        {
            transform.Rotate(new Vector3(0f, 50f * Time.deltaTime, 0f));
            if(scanTimer > 0f)
            {
                scanTimer -= Time.deltaTime;
            }
            else
            {
                RaycastHit hitInfo;
                if(Physics.Raycast(transform.position, transform.forward, out hitInfo, 10f, layerMask))
                {
                    if(hitInfo.transform.gameObject.tag == "Player")
                    {
                        Fire();
                        scanTimer = TIME_BETWEEN_FIRES;
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();
        deathInstance.transform.position = transform.position;
        deathInstance.SetActive(true);
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    private const float RESPAWN_TIMER = 15f;

    private float inactiveTime;

    private GameObject enemy;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (enemy == null)
            enemy = transform.GetChild(0).gameObject;

        if (enemy.activeInHierarchy == false)
        {
            inactiveTime += Time.deltaTime;
            if (inactiveTime > RESPAWN_TIMER)
            {
                enemy.SetActive(true);
                enemy.transform.localPosition = Vector3.zero;
                inactiveTime = 0f;
            }
            

        }

	}
}

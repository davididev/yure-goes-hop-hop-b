﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : MonoBehaviour {

    public TextAsset dialogueScript;
    public bool onlyPlayOnce = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            FindObjectOfType<DialogueHandler>().StartEvent(dialogueScript);
            if (onlyPlayOnce)
                Destroy(this.gameObject);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Just a simpel script to make a texture scroll.  Attach this to the game object
/// that has a renderer and a material with a diffuse texture.  This will not
/// scroll normal, specular, or any other kinds of texture maps.  Just diffuse.
/// </summary>
public class ScrollingTexture : MonoBehaviour {

    public float scrollXPerSec = 0.5f;
    public float scrollYPerSec = 0.25f;

    private Vector2 currentScroll = Vector2.zero;
    private Renderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();

    }
	
	// Update is called once per frame
	void Update () {
        currentScroll.x += scrollXPerSec * Time.deltaTime;
        currentScroll.y += scrollYPerSec * Time.deltaTime;
        if (currentScroll.x < 0f)
            currentScroll.x += 1f;
        if (currentScroll.x > 1f)
            currentScroll.x -= 1f;
        if (currentScroll.y < 0f)
            currentScroll.y += 1f;
        if (currentScroll.y > 1f)
            currentScroll.y -= 1f;

        rend.material.mainTextureOffset = currentScroll;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudPlatform : MonoBehaviour {

    public Animator anim;
    public float offset = 1f;
    public ParticleSystem sys;

	// Use this for initialization
	void Start () {
        anim.SetFloat("delayTimeScale", 1f / offset);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MakeRain()
    {
        sys.Play();
    }
}

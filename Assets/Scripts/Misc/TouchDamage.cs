﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchDamage : MonoBehaviour {

    public int damageAmount = 2;
    public UnityEvent onTouchGeo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ControlHit(ControllerCollisionMessage hit)
    {
        try
        {
            Health h = hit.caster.GetComponent<Health>();
            h.Damage(damageAmount);
            onTouchGeo.Invoke();
        }
        catch(System.Exception e)
        {
            Debug.Log("<Color=red>Error on touch damage: " + e.ToString() + "</color>");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        onTouchGeo.Invoke();
    }
}

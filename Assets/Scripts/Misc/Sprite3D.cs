﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Attach this to a Quad or Plane (preferably a quad) to create a retro-style sprite 
/// in 3D space.
/// 
/// Locking refers to the rotation eulers.  For instance, if you want a Doom-style
/// sprite that stays upright, lock everything but Y.
/// 
/// The textures do not have to be set  for this script to work
/// but can if you wish to animate the component.
/// </summary>
public class Sprite3D : MonoBehaviour {

    public bool lockX = false;
    public bool lockY = false;
    public bool lockZ = false;
    private Transform head;

    [SerializeField]
    private Texture[] textures;
    [SerializeField]
    private float delayBetweenTextures = 0.1f;

    private float textureTimer = 0f;
    private int currentTextureID = 0;
    MeshRenderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<MeshRenderer>();
        if (textures.Length != 0f && rend != null)
            rend.material.mainTexture = textures[0];

        head = Camera.main.transform;
    }
    /*
    void OnValidate()
    {
        MeshRenderer rend = GetComponent<MeshRenderer>();
        if (texture != null && rend != null)
            rend.material.mainTexture = texture;
    }
	*/
	// Update is called once per frame
	void Update () {
        Quaternion q1 = Quaternion.LookRotation(transform.position - head.position);
        Vector3 currentAngles = transform.eulerAngles;
        Vector3 newAngles = q1.eulerAngles;

        //If any axises are locked, don't rotate (go back to the angles BEFORE you rotated)
        if (lockX)  
            newAngles.x = currentAngles.x;
        if (lockY)
            newAngles.y = currentAngles.y;
        if (lockZ)
            newAngles.z = currentAngles.z;

        transform.eulerAngles = newAngles;

        if (textures.Length > 0)  //If there are no textures, do not animate.
        {
            textureTimer += Time.deltaTime;
            if (textureTimer > delayBetweenTextures)
            {
                textureTimer -= delayBetweenTextures;
                currentTextureID++;
                if (currentTextureID >= textures.Length)
                    currentTextureID = 0;

                rend.material.mainTexture = textures[currentTextureID];
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

/// <summary>
/// Attach this to a button and when the panel is set to Active, 
/// this button will be selected by the Event System.
/// </summary>

public class SetDefaultMyButton : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		StartCoroutine(lol());
	}

	void OnDisable()
	{
		StopAllCoroutines();
	}
	
	IEnumerator lol()
	{
		yield return new WaitForSecondsNoTimeScale(0.1f);
		EventSystem.current.SetSelectedGameObject(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if(EventSystem.current.currentSelectedGameObject == null)
		{
			EventSystem.current.SetSelectedGameObject(this.gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decal : MonoBehaviour {

    private Projector projector;
    public enum KillType { SHRINK, INSTANT_HIDE, SPIN_AND_SHRINK, SPIN_AND_HIDE}

	// Use this for initialization
	void Start () {
		
	}

    /// <summary>
    /// After the decal is instantied (or enabled), use this function.
    /// </summary>
    /// <param name="pos">Position to spawn (should come from Collision class)</param>
    /// <param name="normal">Normal of the surface ((should come from Collision class)</param>
    /// <param name="mat">Material of the decal (should be of type Projector)</param>
    /// <param name="size">Square size in meters</param>
    /// <param name="delayBeforeKill">How long to wait until fadeout/shrink</param>
    /// <param name="delayOnKillType">How long the fadeout/shrink should last</param>
    /// <param name="killType">How it should transition away</param>
    public void SetupDecal(Vector3 pos, Vector3 normal, Material mat, float size, float delayBeforeKill, float delayOnKillType, KillType killType)
    {
        transform.forward = -normal;
        transform.position = pos + (normal * 0.12f);

        if (projector == null)
            projector = GetComponent<Projector>();
        projector.material = mat;
        projector.orthographicSize = size * 0.5f;
        setSpin(0f);
        StartCoroutine(Fadeout(killType, delayBeforeKill, delayOnKillType));
    }

    IEnumerator Fadeout(KillType killType, float delay1, float delay2)
    {
        yield return new WaitForSeconds(delay1);

        if(killType == KillType.INSTANT_HIDE)
        {
            //Debug.Log("Fading out");
            //iTween.ValueTo(gameObject, iTween.Hash("onupdate", "setAlpha", "from", 1f, "to", 0f, "time", delay2));
        }
        if(killType == KillType.SHRINK)
        {
            iTween.ValueTo(gameObject, iTween.Hash("onupdate", "setSize", "from", projector.orthographicSize, "to", 0.1f, "time", delay2));
        }
        if (killType == KillType.SPIN_AND_HIDE)
        {
            iTween.ValueTo(gameObject, iTween.Hash("onupdate", "setSpin", "from", 0f, "to", 355f, "time", delay2));
        }
        if (killType == KillType.SPIN_AND_SHRINK)
        {
            iTween.ValueTo(gameObject, iTween.Hash("onupdate", "setSize", "from", projector.orthographicSize, "to", 0.01f, "time", delay2));
            iTween.ValueTo(gameObject, iTween.Hash("onupdate", "setSpin", "from", 0f, "to", 355f, "time", delay2));
        }

        yield return new WaitForSeconds(delay2);
        iTween.Stop(gameObject);
        gameObject.SetActive(false);
    }

    //iTween helper function, sets the size
    void setSize(float s)
    {
        projector.orthographicSize = s;
    }
    

    //iTween helper function, sets the z rotation
    void setSpin(float a)
    {
        Vector3 ang = transform.eulerAngles;
        ang.z = a;
        transform.eulerAngles = ang;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

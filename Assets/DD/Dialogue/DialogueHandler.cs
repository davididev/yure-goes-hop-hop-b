﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

public class DialogueHandler : MonoBehaviour {

    public static Transform dialogueTarget;

    public GameObject messageBoxContainer;
    private Image messageImage;
    public Image characterImage, flashImage;
    public TMPro.TextMeshProUGUI message;
    public GameObject[] choiceOverlays;
    public int RUNNING_STACKS {
        get { return currentRunning.Count; } }


    public static AudioSource dialogueStringSrc;  //The source of regular dialogue

    public List<DialogueItem> currentRunning = new List<DialogueItem>();
    public DialogueHolder currentDialogue = null;
    private int currentID = -1;
    public static bool IS_RUNNING = false;

    // Use this for initialization
    void Start () {
        messageImage = messageBoxContainer.GetComponent<Image>();
        dialogueStringSrc = gameObject.AddComponent<AudioSource>();
        dialogueStringSrc.minDistance = 25f; 
        dialogueStringSrc.loop = false;
    }

    public void SetFlashColor(Color c)
    {
        flashImage.color = c;
    }

    /// <summary>
    /// Change the message Y.
    /// </summary>
    /// <param name="y">0 is showing, -192 is hidden.</param>
    public void SetMessageY(float y)
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", messageImage.rectTransform.anchoredPosition.y, "to", y, "time", 0.25f, "onupdate", "SY"));
    }

    private void SY(float f)
    {
        Vector3 v = messageImage.rectTransform.anchoredPosition;
        v.y = f;
        messageImage.rectTransform.anchoredPosition = new Vector3(0f, v.y, 0f);
    }

    public static Vector2 joystick1;

    public static Dictionary<string, float> variables = new Dictionary<string, float>();

    void OnDestroy()
    {
        dialogueTarget = null;
        variables.Clear();
        IS_RUNNING = false;
        
    }
	
	/// <summary>
    /// Get a temporary variable for the dialogue handler.
    /// </summary>
    /// <param name="varName">NAme of temporary variable</param>
    /// <returns></returns>
    public static float GetVar(string varName)
    {
        float f;
        if (variables.TryGetValue(varName, out f))
        {
            return f;
        }
        else
            return 0f;
    }

    /// <summary>
    /// Set a temporary variable for the dialogue handler.  Can only be read in if statements and dialogue.
    /// </summary>
    /// <param name="varName">Variable name to set</param>
    /// <param name="varValue">Float value to set it to.</param>
    public static void SetVar(string varName, float varValue)
    {
        if (variables.ContainsKey(varName))
            variables.Remove(varName);

        variables.Add(varName, varValue);
    }

    /// <summary>
    /// Pass in a string and get out the values of any variables that have been added.  Additionally checks for colors and new lines.
    /// </summary>
    /// <param name="str">String to check for variable names.</param>
    /// <returns></returns>
    public static string VariablesToString(string str)
    {
        str = str.Replace("\\c[0]", "</color>");
        str = str.Replace("\\c[1]", "<color=#00ffff>");
        str = str.Replace("\\c[2]", "<color=#ffff00>");
        str = str.Replace("\\n", "\n");
        Dictionary<string, float>.Enumerator e1 = DialogueHandler.variables.GetEnumerator();
        while(e1.MoveNext())
        {
            str = str.Replace(e1.Current.Key, e1.Current.Value.ToString());
        }
        return str;
    }

    private bool waitingForBreakpoint = false;
	// Update is called once per frame
	void Update () {
	    if(IS_RUNNING)
        {
            joystick1 = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            
            if (waitingForBreakpoint == false)
            {
                
                //Keep going until you reach a breakpoint or the end
                currentID++;
                if(currentID >= currentDialogue.items.Length)  //At the end
                {
                    EndEvent();
                    return;
                }
                
                
                Debug.Log("<B>ADDING EVENT" + currentDialogue.items[currentID].ToString() + "</b>");
                StartCoroutine(currentDialogue.items[currentID].Run());
                currentRunning.Add(currentDialogue.items[currentID]);
                if (currentDialogue.items[currentID].breakpoint == true)
                {
                    Debug.Log("<i>Halt, breakpoint at line " + currentID + ": " + currentDialogue.items[currentID].ToString() + " </i>");
                    waitingForBreakpoint = true;
                }
            }
            else
            {
                //One of the items had a breakpoint.  Wait until all of them are completed before continuing.
                /*
                List<DialogueItem>.Enumerator e1 = currentRunning.GetEnumerator();
                while (e1.MoveNext())
                {
                    if (e1.Current.completed == true)
                    {
                        currentRunning.Remove(e1.Current);
                        break;
                    }
                }
                */
                bool b = true;
                while(b == true) { b = RemoveCurrentRunning(); }

                if (currentRunning.Count == 0) //All items removed
                {
                    waitingForBreakpoint = false;
                    if(gotoL != -1)  //The breakpoint called for a goto line
                    {
                        currentID = gotoL - 1;
                        gotoL = -1;
                        waitingForBreakpoint = false;
                    }
                }
            }
        }
	}
    
    /// <summary>
    /// Remove any currentRunning scripts that are finished (should be called multiple times)
    /// </summary>
    /// <returns>"true" if there  are still some that need to be removed, else returns "false"</returns>
    public bool RemoveCurrentRunning()
    {
        List<DialogueItem>.Enumerator e1 = currentRunning.GetEnumerator();
        while (e1.MoveNext())
        {
            if (e1.Current.completed == true)
            {
                currentRunning.Remove(e1.Current);
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Write the global data variables to variable system (should be called before Dialogue Handler starts)
    /// </summary>
    void SetGlobalVars()
    {
        for (int i = 0; i < GlobalData.instance.worldsCompleted.Length; i++)
        {
            variables.Remove("%w" + i);
            float f = 0f;
            if (GlobalData.instance.worldsCompleted[i] == true)
                f = 1f;
            variables.Add("%w" + i, f);
        }

        variables.Remove("%easter");
        variables.Add("%easter", (float)GlobalData.instance.easterEggProgress);
    }


    /// <summary>
    /// Get the global data variables from variable system (should be called at the end of Dialogue Hansdler)
    /// </summary>
    void GetGlobalVars()
    {
        for (int i = 0; i < GlobalData.instance.worldsCompleted.Length; i++)
        {
            float f2 = 0f;
            if(variables.TryGetValue("%w" + i, out f2) == true)
            {
                bool b = false;
                if (f2 == 1f)
                    b = true; 
                GlobalData.instance.worldsCompleted[i] = b;
            }
        }

        float f3 = 0f;
        if(variables.TryGetValue("%easter", out f3))
        {
            GlobalData.instance.easterEggProgress = Mathf.FloorToInt(f3);
        }

    }
    /// <summary>
    /// Start event based on text asset.
    /// </summary>
    /// <param name="file"></param>
    public void StartEvent(TextAsset file)
    {
        SetGlobalVars();
        var serializer = new XmlSerializer(typeof(DialogueHolder)); 
        currentDialogue = serializer.Deserialize(new StringReader(file.text)) as DialogueHolder;
        LoadedDialogueHolder();
    }

    /// <summary>
    /// Start event based on classes
    /// </summary>
    /// <param name="items"></param>
    public void StartEvent(DialogueItem[] items)
    {
        SetGlobalVars();
        DialogueHolder dh = new DialogueHolder();
        dh.items = items;
        currentDialogue = dh;
        LoadedDialogueHolder();
    }

    /// <summary>
    /// internal function.  Use after DialogueHolder is loaded.
    /// </summary>
    void LoadedDialogueHolder()
    {
        waitingForBreakpoint = false;
        currentID = -1;
        IS_RUNNING = true;

    }

    //When all the breakpoints are over, go to this line
    private int gotoL = -1;
    public void GotoLine(int i)
    {
        gotoL = i;
    }
    
    public void EndEvent()
    {
        Debug.Log("<B>END EVENT</b>");
        GetGlobalVars();
        currentRunning.Clear();
        currentID = -1;
        IS_RUNNING = false;
    }
}

﻿using System.Collections;
using UnityEngine;

public class DialogueSendMessage : DialogueItem
{

    public string actorName = "Player";
    public string methodToCall = "PlayerMethod";
    public string argument = "";

    public DialogueSendMessage()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();

        Actor a1;
        if (Actor.actors.TryGetValue(actorName, out a1))
        {
            SendArg(a1);
        }

        completed = true;
    }

    void SendArg(Actor a1)
    {
        
        int possibleIntArg;
        if (int.TryParse(argument, out possibleIntArg))
        {
            a1.gameObject.SendMessage(methodToCall, possibleIntArg);
            return;
        }
        float possibleFloatArg;
        if (float.TryParse(argument, out possibleFloatArg))
        {
            a1.gameObject.SendMessage(methodToCall, possibleFloatArg);
            return;
        }

        a1.gameObject.SendMessage(methodToCall, argument, SendMessageOptions.DontRequireReceiver);
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Actor");
        actorName = GUILayout.TextField(actorName);
        GUILayout.Label("Method Name");
        methodToCall = GUILayout.TextField(methodToCall);
        GUILayout.Label("Argument (leave blank if none)");
        argument = GUILayout.TextField(argument);
    }

    public override string ToString()
    {
        return "Send message";
    }
}
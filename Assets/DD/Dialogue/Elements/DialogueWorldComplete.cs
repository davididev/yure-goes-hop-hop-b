﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System.Xml.Serialization;

[System.Serializable]
public class DialogueWorldComplete : DialogueItem {

    public int worldID = 1;

    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);
        worldID = IntField("World ID", worldID);
    }

    public override IEnumerator Run()
    {
        yield return base.Run();


        WinnerUI.keyNum = worldID;
        handle.SetMessageY(-192f);
        yield return new WaitForSeconds(0.15f);
        if (worldID == 4)
        {
            LoadingUI.scene = "Epilogue";
            GlobalData.instance.worldsCompleted[3] = true;
            GlobalData.instance.SaveFile();
        }
        else
            LoadingUI.scene = "Winner";
		GameObject.FindObjectOfType<PlayerUI>().LoadNewLevel();
		

        completed = true;
    }

    public string ToString()
    {
        return "World Complete";
    }
}

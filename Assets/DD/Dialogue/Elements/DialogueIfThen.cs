﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogueIfThen : DialogueItem {

    public int gotoIfTrue = 0;
    public int gotoIfFalse = 0;
    public string compareLeft = "%var1";
    public string compareRight = "%var2";
    public enum CONDITION { EQUALS, LESS_THAN, MORE_THAN, LESS_EQUAL_THEN, MORE_EQUAL_THEN, NOT_EQUAL_TO};
    public CONDITION condition = CONDITION.EQUALS;

	public DialogueIfThen()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        completed = true;
        compareLeft = DialogueHandler.VariablesToString(compareLeft);
        compareRight = DialogueHandler.VariablesToString(compareRight);

        

        bool returnsTrue = false;
        float leftSide;
        float rightSide;
        if (float.TryParse(compareLeft, out leftSide) == false)
        {
            Debug.Log("<color=red>Could not take var " + compareLeft + " from list.  Set to 0.</color>");
            leftSide = 0f;
        }
        if (float.TryParse(compareRight, out rightSide) == false)
        {
            Debug.Log("<color=red>Could not take var " + compareRight + " from list.  Set to 0.</color>");
            rightSide = 0f;
        }


        if (condition == CONDITION.EQUALS && leftSide == rightSide)
            returnsTrue = true;
        if (condition == CONDITION.LESS_EQUAL_THEN && leftSide <= rightSide)
            returnsTrue = true;
        if (condition == CONDITION.LESS_THAN && leftSide < rightSide)
            returnsTrue = true;
        if (condition == CONDITION.MORE_EQUAL_THEN && leftSide >= rightSide)
            returnsTrue = true;
        if (condition == CONDITION.MORE_THAN && leftSide > rightSide)
            returnsTrue = true;
        if (condition == CONDITION.NOT_EQUAL_TO && leftSide != rightSide)
            returnsTrue = true;

        if (returnsTrue)
            this.handle.GotoLine(gotoIfTrue);
        else
            this.handle.GotoLine(gotoIfFalse);

        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("If");
        compareLeft = GUILayout.TextField(compareLeft);
        condition = (CONDITION) this.EnumPopup("Condition: ", condition);
        GUILayout.Label("Than");
        compareRight = GUILayout.TextField(compareRight);

        GUILayout.Space(1f);
        gotoIfTrue = this.IntField("If true goto line ", gotoIfTrue);
        gotoIfFalse = this.IntField("If false goto line ", gotoIfFalse);
    }
    public override string ToString()
    {
        return "If/Then/Else";
    }
}

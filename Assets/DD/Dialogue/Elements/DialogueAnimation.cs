﻿using UnityEngine;
using System.Collections;

public class DialogueAnimation : DialogueItem
{
    public enum ANIM_TYPE { BOOL, FLOAT, INT, TRIGGER}
    public ANIM_TYPE animType = ANIM_TYPE.BOOL;
    public string actorName = "";
    public string varNameToSet = "";
    public bool boolValue = true;
    public float floatValue = 0f;
    public int intValue = 0;

    public override IEnumerator Run()
    {
        yield return base.Run();
        Actor myActor;
        if(Actor.actors.TryGetValue(actorName, out myActor))
        {
            Animator anim = myActor.GetComponent<Animator>();
            if (anim != null)
            {
                if (animType == ANIM_TYPE.BOOL)
                    anim.SetBool(varNameToSet, boolValue);
                if (animType == ANIM_TYPE.FLOAT)
                    anim.SetFloat(varNameToSet, floatValue);
                if (animType == ANIM_TYPE.INT)
                    anim.SetInteger(varNameToSet, intValue);
				if (animType == ANIM_TYPE.TRIGGER)
                    anim.SetTrigger(varNameToSet); 
            }
            else
            {
                Debug.Log("<color=red>Actor " + myActor + " does not have animator.</color>");
            }
        }
        else
        {
            Debug.Log("<color=red>Could not load actor name " + myActor + ".</color>");
        }
        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Actor name");
        actorName = GUILayout.TextField(actorName);
        GUILayout.Label("Animator variable");
        varNameToSet = GUILayout.TextField(varNameToSet);
        
        animType = (ANIM_TYPE) this.EnumPopup("Variable type", animType);

        if (animType == ANIM_TYPE.BOOL)
            boolValue = GUILayout.Toggle(boolValue, "Value: ");
        if (animType == ANIM_TYPE.FLOAT)
            floatValue = this.FloatField("Value: ", floatValue);
        if (animType == ANIM_TYPE.INT)
            intValue = this.IntField("Value: ", intValue);
    }

    public override string ToString()
    {
        return "Animation";
    }
}

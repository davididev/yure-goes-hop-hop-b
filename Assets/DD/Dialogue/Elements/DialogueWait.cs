﻿
using UnityEngine;
using System.Collections;

public class DialogueWait : DialogueItem
{
    public float delay = 0f;
    public DialogueWait()
    {
        
    }

    public override IEnumerator Run()
    {
        /*
        float firstTime = Time.realtimeSinceStartup;
        while(Time.realtimeSinceStartup < firstTime + delay)
        {
            yield return null;
        }
        */
        
        if (Mathf.Approximately(delay, 0f))
            breakpoint = true;
        else
            breakpoint = false;

        yield return new WaitForSecondsNoTimeScale(delay);
        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        delay = FloatField("How long to wait?", delay);
        GUILayout.Label("Keep at zero to make this a breakpoint.");
    }

    public override string ToString()
    {
        return "Wait for completion";
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class DialogueString : DialogueItem {

    public string characterName = "";
    public string textToDisplay = "";
    public string sound = "";
    public bool cantSkip = false;

    private const float WIDTH = 400f;
    private const float HEIGHT = 300f;
    public static string lastCharacterName = "";

    public DialogueString()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();

        handle.SetMessageY(0f);
        
        handle.characterImage.sprite = Resources.Load<Sprite>("Face/" + characterName);
        string str = textToDisplay;

        DialogueHandler.dialogueStringSrc.clip = Resources.Load<AudioClip>("Face/" + sound);

        str = DialogueHandler.VariablesToString(str);

        int i = 0;
        while (i < str.Length)
        {
            if (DialogueHandler.dialogueStringSrc.isPlaying == false)
            {
                DialogueHandler.dialogueStringSrc.pitch = Random.Range(0.5f, 1.5f);
                DialogueHandler.dialogueStringSrc.Play();
            }
            if (i >= str.Length)
                i = str.Length - 1;

            if (str[i] == '<')
            {
                i = str.IndexOf('>', i) + 1;
            }
            
            handle.message.text = str.Substring(0, i);
            yield return new WaitForSecondsNoTimeScale(0.05f);
            i++;

            if (cantSkip == false)
            {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.Space))
                    i = str.Length;
            }
        }

        handle.message.text = str;


        
        

        //Wait until the button is up
        while (Input.GetKeyDown(KeyCode.Space) == true)
        {
            yield return null;
        }

        //Wait until it's down again
        while (Input.GetKeyDown(KeyCode.Space) == false)
        {
            yield return null;
        }

        yield return new WaitForSecondsNoTimeScale(0.05f);
        completed = true;

    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Character name");
        characterName = GUILayout.TextField(characterName);
        GUILayout.Label("Sounds");
        sound = GUILayout.TextField(sound);
        GUILayout.Label("Text to display");
        textToDisplay = GUILayout.TextField(textToDisplay);


        cantSkip = GUILayout.Toggle(cantSkip, "Can't Skip?");
    }

    //Needed for the editor.  Returns the object tpye
    public override string ToString()
    {
        return "Talk (" + characterName + ")";
    }
}

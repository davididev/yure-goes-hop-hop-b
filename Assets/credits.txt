Graphics:
(opengameart.org) Textures by: kleenstudios, The Chayed - KIIRA, samuncle
(opengameart.org) Textures by: Danimal, AntumDeluge, MrCraft Animation
(opengameart.org) Trees by: b_o,
Low Poly Cat by volkanongun @ sketchfab.com
*Graphics:
Comic Book by Pixel Sagas @ dafont.com
Old English by Microsoft
Sparkle by Julien @ opengameart.org
Digital 7 by Style-7  @ dafont.com
*Graphics:
Skybox by G.E. Team / Bright Shining Star on Asset Store
Fire Particle by KnoblePersona @ opengameart.org
Lava texture by LuminousDragonGames @ opengameart.org
Majestical textures by Adam Foster, Randy Reddig, and Robert Yang
*Graphics:
Keycard by codeinfernogames @ opengameart.org
Dragon model base by Bitiquinho @ opengameart.com

*Unity Packages:
Probuilder by Unity Technologies
iTween by Pixel Placement 
TextMeshPro by Unity Technologies
Light Probes generator by Sam Hocevar

*Audio:
Most music/sound from freesfx.co.uk
Title music by cynicmusic @ opengameart.org
Sky music by extenz @ opengameart.org
*Audio:
Mr Blue Sky by Snabisch @ opengameart.org
Chapel of Hate by FoxSynergy @ opengameart.org
Fire spell by Bart K. @ opengameart.org
Mouse Squeak by AntumDeluge @ opengameart.org
*Audio:
Inner moon (Alexandr Zhelanov, https://soundcloud.com/alexandr-zhelanov)
Outer Moon (Alexandr Zhelanov, https://soundcloud.com/alexandr-zhelanov)
Random SFX by Écrivain @ opengameart.org

*Characters used:
xblueashesx @ furaffinity
yure16 @ furaffinity
alenthehuskie @ furaffinity
mandaliet @ furcadia
*Characters used:
Prince River @ furcadia
marshie @ furaffinity
Rockwellfox @ furaffinity 
GreyMousey @ furaffinity
*Characters used:
Terra Wolf @ Furcadia